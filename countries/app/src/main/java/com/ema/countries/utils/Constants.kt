package com.ema.countries.utils

const val BASE_URL = "https://restcountries.com/v3.1/"

//db
const val COUNTRY_TABLE = "country_table"
const val COUNTRY_DATABASE="country_database"
const val FAVORITE_TABLE="favorite_table"
