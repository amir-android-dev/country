package com.ema.countries.data.model


import com.ema.countries.data.model.ResponseAllCountries.ResponseAllCountriesItem
import com.google.gson.annotations.SerializedName

class ResponseAllCountries : ArrayList<ResponseAllCountriesItem>(){
    data class ResponseAllCountriesItem(
        @SerializedName("altSpellings")
        val altSpellings: List<String?>?,
        @SerializedName("area")
        val area: Double?, // 430.0
        @SerializedName("borders")
        val borders: List<String>,
        @SerializedName("capital")
        val capital: List<String?>?,
        @SerializedName("capitalInfo")
        val capitalInfo: CapitalInfo?,
        @SerializedName("car")
        val car: Car?,
        @SerializedName("cioc")
        val cioc: String?, // BAR
        @SerializedName("coatOfArms")
        val coatOfArms: CoatOfArms?,
        @SerializedName("continents")
        val continents: List<String?>?,
        @SerializedName("currencies")
        val currencies: Currencies?,
        @SerializedName("demonyms")
        val demonyms: Demonyms?,
        @SerializedName("fifa")
        val fifa: String?, // BRB
        @SerializedName("flag")
        val flag: String?, // 🇧🇧
        @SerializedName("flags")
        val flags: Flags?,
        @SerializedName("idd")
        val idd: Idd?,
        @SerializedName("independent")
        val independent: Boolean?, // true
        @SerializedName("landlocked")
        val landlocked: Boolean?, // false
        @SerializedName("languages")
        val languages: Languages?,
        @SerializedName("latlng")
        val latlng: List<Double?>?,
        @SerializedName("maps")
        val maps: Maps?,
        @SerializedName("name")
        val name: Name?,
        @SerializedName("population")
        val population: Int?, // 287371
        @SerializedName("postalCode")
        val postalCode: PostalCode?,
        @SerializedName("region")
        val region: String?, // Americas
        @SerializedName("startOfWeek")
        val startOfWeek: String?, // monday
        @SerializedName("status")
        val status: String?, // officially-assigned
        @SerializedName("subregion")
        val subregion: String?, // Caribbean
        @SerializedName("timezones")
        val timezones: List<String?>?,
        @SerializedName("tld")
        val tld: List<String?>?,
        @SerializedName("translations")
        val translations: Translations?,
        @SerializedName("unMember")
        val unMember: Boolean? // true
    ) {
        data class CapitalInfo(
            @SerializedName("latlng")
            val latlng: List<Double?>?
        )
    
        data class Car(
            @SerializedName("side")
            val side: String?, // left
            @SerializedName("signs")
            val signs: List<String?>?
        )
    
        data class CoatOfArms(
            @SerializedName("png")
            val png: String?, // https://mainfacts.com/media/images/coats_of_arms/bb.png
            @SerializedName("svg")
            val svg: String? // https://mainfacts.com/media/images/coats_of_arms/bb.svg
        )
    
        data class Currencies(
            @SerializedName("AED")
            val aED: AED?,
            @SerializedName("AFN")
            val aFN: AFN?,
            @SerializedName("ALL")
            val aLL: ALL?,
            @SerializedName("AMD")
            val aMD: AMD?,
            @SerializedName("ANG")
            val aNG: ANG?,
            @SerializedName("AOA")
            val aOA: AOA?,
            @SerializedName("ARS")
            val aRS: ARS?,
            @SerializedName("AUD")
            val aUD: AUD?,
            @SerializedName("AWG")
            val aWG: AWG?,
            @SerializedName("AZN")
            val aZN: AZN?,
            @SerializedName("BAM")
            val bAM: BAM?,
            @SerializedName("BBD")
            val bBD: BBD?,
            @SerializedName("BDT")
            val bDT: BDT?,
            @SerializedName("BGN")
            val bGN: BGN?,
            @SerializedName("BHD")
            val bHD: BHD?,
            @SerializedName("BIF")
            val bIF: BIF?,
            @SerializedName("BMD")
            val bMD: BMD?,
            @SerializedName("BND")
            val bND: BND?,
            @SerializedName("BOB")
            val bOB: BOB?,
            @SerializedName("BRL")
            val bRL: BRL?,
            @SerializedName("BSD")
            val bSD: BSD?,
            @SerializedName("BTN")
            val bTN: BTN?,
            @SerializedName("BWP")
            val bWP: BWP?,
            @SerializedName("BYN")
            val bYN: BYN?,
            @SerializedName("BZD")
            val bZD: BZD?,
            @SerializedName("CAD")
            val cAD: CAD?,
            @SerializedName("CDF")
            val cDF: CDF?,
            @SerializedName("CHF")
            val cHF: CHF?,
            @SerializedName("CKD")
            val cKD: CKD?,
            @SerializedName("CLP")
            val cLP: CLP?,
            @SerializedName("CNY")
            val cNY: CNY?,
            @SerializedName("COP")
            val cOP: COP?,
            @SerializedName("CRC")
            val cRC: CRC?,
            @SerializedName("CUC")
            val cUC: CUC?,
            @SerializedName("CUP")
            val cUP: CUP?,
            @SerializedName("CVE")
            val cVE: CVE?,
            @SerializedName("CZK")
            val cZK: CZK?,
            @SerializedName("DJF")
            val dJF: DJF?,
            @SerializedName("DKK")
            val dKK: DKK?,
            @SerializedName("DOP")
            val dOP: DOP?,
            @SerializedName("DZD")
            val dZD: DZD?,
            @SerializedName("EGP")
            val eGP: EGP?,
            @SerializedName("ERN")
            val eRN: ERN?,
            @SerializedName("ETB")
            val eTB: ETB?,
            @SerializedName("EUR")
            val eUR: EUR?,
            @SerializedName("FJD")
            val fJD: FJD?,
            @SerializedName("FKP")
            val fKP: FKP?,
            @SerializedName("FOK")
            val fOK: FOK?,
            @SerializedName("GBP")
            val gBP: GBP?,
            @SerializedName("GEL")
            val gEL: GEL?,
            @SerializedName("GGP")
            val gGP: GGP?,
            @SerializedName("GHS")
            val gHS: GHS?,
            @SerializedName("GIP")
            val gIP: GIP?,
            @SerializedName("GMD")
            val gMD: GMD?,
            @SerializedName("GNF")
            val gNF: GNF?,
            @SerializedName("GTQ")
            val gTQ: GTQ?,
            @SerializedName("GYD")
            val gYD: GYD?,
            @SerializedName("HKD")
            val hKD: HKD?,
            @SerializedName("HNL")
            val hNL: HNL?,
            @SerializedName("HTG")
            val hTG: HTG?,
            @SerializedName("HUF")
            val hUF: HUF?,
            @SerializedName("IDR")
            val iDR: IDR?,
            @SerializedName("ILS")
            val iLS: ILS?,
            @SerializedName("IMP")
            val iMP: IMP?,
            @SerializedName("INR")
            val iNR: INR?,
            @SerializedName("IQD")
            val iQD: IQD?,
            @SerializedName("IRR")
            val iRR: IRR?,
            @SerializedName("ISK")
            val iSK: ISK?,
            @SerializedName("JEP")
            val jEP: JEP?,
            @SerializedName("JMD")
            val jMD: JMD?,
            @SerializedName("JOD")
            val jOD: JOD?,
            @SerializedName("JPY")
            val jPY: JPY?,
            @SerializedName("KES")
            val kES: KES?,
            @SerializedName("KGS")
            val kGS: KGS?,
            @SerializedName("KHR")
            val kHR: KHR?,
            @SerializedName("KID")
            val kID: KID?,
            @SerializedName("KMF")
            val kMF: KMF?,
            @SerializedName("KPW")
            val kPW: KPW?,
            @SerializedName("KRW")
            val kRW: KRW?,
            @SerializedName("KWD")
            val kWD: KWD?,
            @SerializedName("KYD")
            val kYD: KYD?,
            @SerializedName("KZT")
            val kZT: KZT?,
            @SerializedName("LAK")
            val lAK: LAK?,
            @SerializedName("LBP")
            val lBP: LBP?,
            @SerializedName("LKR")
            val lKR: LKR?,
            @SerializedName("LRD")
            val lRD: LRD?,
            @SerializedName("LSL")
            val lSL: LSL?,
            @SerializedName("LYD")
            val lYD: LYD?,
            @SerializedName("MAD")
            val mAD: MAD?,
            @SerializedName("MDL")
            val mDL: MDL?,
            @SerializedName("MGA")
            val mGA: MGA?,
            @SerializedName("MKD")
            val mKD: MKD?,
            @SerializedName("MMK")
            val mMK: MMK?,
            @SerializedName("MNT")
            val mNT: MNT?,
            @SerializedName("MOP")
            val mOP: MOP?,
            @SerializedName("MRU")
            val mRU: MRU?,
            @SerializedName("MUR")
            val mUR: MUR?,
            @SerializedName("MVR")
            val mVR: MVR?,
            @SerializedName("MWK")
            val mWK: MWK?,
            @SerializedName("MXN")
            val mXN: MXN?,
            @SerializedName("MYR")
            val mYR: MYR?,
            @SerializedName("MZN")
            val mZN: MZN?,
            @SerializedName("NAD")
            val nAD: NAD?,
            @SerializedName("NGN")
            val nGN: NGN?,
            @SerializedName("NIO")
            val nIO: NIO?,
            @SerializedName("NOK")
            val nOK: NOK?,
            @SerializedName("NPR")
            val nPR: NPR?,
            @SerializedName("NZD")
            val nZD: NZD?,
            @SerializedName("OMR")
            val oMR: OMR?,
            @SerializedName("PAB")
            val pAB: PAB?,
            @SerializedName("PEN")
            val pEN: PEN?,
            @SerializedName("PGK")
            val pGK: PGK?,
            @SerializedName("PHP")
            val pHP: PHP?,
            @SerializedName("PKR")
            val pKR: PKR?,
            @SerializedName("PLN")
            val pLN: PLN?,
            @SerializedName("PYG")
            val pYG: PYG?,
            @SerializedName("QAR")
            val qAR: QAR?,
            @SerializedName("RON")
            val rON: RON?,
            @SerializedName("RSD")
            val rSD: RSD?,
            @SerializedName("RUB")
            val rUB: RUB?,
            @SerializedName("RWF")
            val rWF: RWF?,
            @SerializedName("SAR")
            val sAR: SAR?,
            @SerializedName("SBD")
            val sBD: SBD?,
            @SerializedName("SCR")
            val sCR: SCR?,
            @SerializedName("SDG")
            val sDG: SDG?,
            @SerializedName("SEK")
            val sEK: SEK?,
            @SerializedName("SGD")
            val sGD: SGD?,
            @SerializedName("SHP")
            val sHP: SHP?,
            @SerializedName("SLL")
            val sLL: SLL?,
            @SerializedName("SOS")
            val sOS: SOS?,
            @SerializedName("SRD")
            val sRD: SRD?,
            @SerializedName("SSP")
            val sSP: SSP?,
            @SerializedName("STN")
            val sTN: STN?,
            @SerializedName("SYP")
            val sYP: SYP?,
            @SerializedName("SZL")
            val sZL: SZL?,
            @SerializedName("THB")
            val tHB: THB?,
            @SerializedName("TJS")
            val tJS: TJS?,
            @SerializedName("TMT")
            val tMT: TMT?,
            @SerializedName("TND")
            val tND: TND?,
            @SerializedName("TOP")
            val tOP: TOP?,
            @SerializedName("TRY")
            val tRY: TRY?,
            @SerializedName("TTD")
            val tTD: TTD?,
            @SerializedName("TVD")
            val tVD: TVD?,
            @SerializedName("TWD")
            val tWD: TWD?,
            @SerializedName("TZS")
            val tZS: TZS?,
            @SerializedName("UAH")
            val uAH: UAH?,
            @SerializedName("UGX")
            val uGX: UGX?,
            @SerializedName("USD")
            val uSD: USD?,
            @SerializedName("UYU")
            val uYU: UYU?,
            @SerializedName("UZS")
            val uZS: UZS?,
            @SerializedName("VES")
            val vES: VES?,
            @SerializedName("VND")
            val vND: VND?,
            @SerializedName("VUV")
            val vUV: VUV?,
            @SerializedName("WST")
            val wST: WST?,
            @SerializedName("XAF")
            val xAF: XAF?,
            @SerializedName("XCD")
            val xCD: XCD?,
            @SerializedName("XOF")
            val xOF: XOF?,
            @SerializedName("XPF")
            val xPF: XPF?,
            @SerializedName("YER")
            val yER: YER?,
            @SerializedName("ZAR")
            val zAR: ZAR?,
            @SerializedName("ZMW")
            val zMW: ZMW?,
            @SerializedName("ZWL")
            val zWL: ZWL?
        ) {
            data class AED(
                @SerializedName("name")
                val name: String?, // United Arab Emirates dirham
                @SerializedName("symbol")
                val symbol: String? // د.إ
            )
    
            data class AFN(
                @SerializedName("name")
                val name: String?, // Afghan afghani
                @SerializedName("symbol")
                val symbol: String? // ؋
            )
    
            data class ALL(
                @SerializedName("name")
                val name: String?, // Albanian lek
                @SerializedName("symbol")
                val symbol: String? // L
            )
    
            data class AMD(
                @SerializedName("name")
                val name: String?, // Armenian dram
                @SerializedName("symbol")
                val symbol: String? // ֏
            )
    
            data class ANG(
                @SerializedName("name")
                val name: String?, // Netherlands Antillean guilder
                @SerializedName("symbol")
                val symbol: String? // ƒ
            )
    
            data class AOA(
                @SerializedName("name")
                val name: String?, // Angolan kwanza
                @SerializedName("symbol")
                val symbol: String? // Kz
            )
    
            data class ARS(
                @SerializedName("name")
                val name: String?, // Argentine peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class AUD(
                @SerializedName("name")
                val name: String?, // Australian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class AWG(
                @SerializedName("name")
                val name: String?, // Aruban florin
                @SerializedName("symbol")
                val symbol: String? // ƒ
            )
    
            data class AZN(
                @SerializedName("name")
                val name: String?, // Azerbaijani manat
                @SerializedName("symbol")
                val symbol: String? // ₼
            )
    
            data class BAM(
                @SerializedName("name")
                val name: String? // Bosnia and Herzegovina convertible mark
            )
    
            data class BBD(
                @SerializedName("name")
                val name: String?, // Barbadian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class BDT(
                @SerializedName("name")
                val name: String?, // Bangladeshi taka
                @SerializedName("symbol")
                val symbol: String? // ৳
            )
    
            data class BGN(
                @SerializedName("name")
                val name: String?, // Bulgarian lev
                @SerializedName("symbol")
                val symbol: String? // лв
            )
    
            data class BHD(
                @SerializedName("name")
                val name: String?, // Bahraini dinar
                @SerializedName("symbol")
                val symbol: String? // .د.ب
            )
    
            data class BIF(
                @SerializedName("name")
                val name: String?, // Burundian franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class BMD(
                @SerializedName("name")
                val name: String?, // Bermudian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class BND(
                @SerializedName("name")
                val name: String?, // Brunei dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class BOB(
                @SerializedName("name")
                val name: String?, // Bolivian boliviano
                @SerializedName("symbol")
                val symbol: String? // Bs.
            )
    
            data class BRL(
                @SerializedName("name")
                val name: String?, // Brazilian real
                @SerializedName("symbol")
                val symbol: String? // R$
            )
    
            data class BSD(
                @SerializedName("name")
                val name: String?, // Bahamian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class BTN(
                @SerializedName("name")
                val name: String?, // Bhutanese ngultrum
                @SerializedName("symbol")
                val symbol: String? // Nu.
            )
    
            data class BWP(
                @SerializedName("name")
                val name: String?, // Botswana pula
                @SerializedName("symbol")
                val symbol: String? // P
            )
    
            data class BYN(
                @SerializedName("name")
                val name: String?, // Belarusian ruble
                @SerializedName("symbol")
                val symbol: String? // Br
            )
    
            data class BZD(
                @SerializedName("name")
                val name: String?, // Belize dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CAD(
                @SerializedName("name")
                val name: String?, // Canadian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CDF(
                @SerializedName("name")
                val name: String?, // Congolese franc
                @SerializedName("symbol")
                val symbol: String? // FC
            )
    
            data class CHF(
                @SerializedName("name")
                val name: String?, // Swiss franc
                @SerializedName("symbol")
                val symbol: String? // Fr.
            )
    
            data class CKD(
                @SerializedName("name")
                val name: String?, // Cook Islands dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CLP(
                @SerializedName("name")
                val name: String?, // Chilean peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CNY(
                @SerializedName("name")
                val name: String?, // Chinese yuan
                @SerializedName("symbol")
                val symbol: String? // ¥
            )
    
            data class COP(
                @SerializedName("name")
                val name: String?, // Colombian peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CRC(
                @SerializedName("name")
                val name: String?, // Costa Rican colón
                @SerializedName("symbol")
                val symbol: String? // ₡
            )
    
            data class CUC(
                @SerializedName("name")
                val name: String?, // Cuban convertible peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CUP(
                @SerializedName("name")
                val name: String?, // Cuban peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class CVE(
                @SerializedName("name")
                val name: String?, // Cape Verdean escudo
                @SerializedName("symbol")
                val symbol: String? // Esc
            )
    
            data class CZK(
                @SerializedName("name")
                val name: String?, // Czech koruna
                @SerializedName("symbol")
                val symbol: String? // Kč
            )
    
            data class DJF(
                @SerializedName("name")
                val name: String?, // Djiboutian franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class DKK(
                @SerializedName("name")
                val name: String?, // Danish krone
                @SerializedName("symbol")
                val symbol: String? // kr
            )
    
            data class DOP(
                @SerializedName("name")
                val name: String?, // Dominican peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class DZD(
                @SerializedName("name")
                val name: String?, // Algerian dinar
                @SerializedName("symbol")
                val symbol: String? // دج
            )
    
            data class EGP(
                @SerializedName("name")
                val name: String?, // Egyptian pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class ERN(
                @SerializedName("name")
                val name: String?, // Eritrean nakfa
                @SerializedName("symbol")
                val symbol: String? // Nfk
            )
    
            data class ETB(
                @SerializedName("name")
                val name: String?, // Ethiopian birr
                @SerializedName("symbol")
                val symbol: String? // Br
            )
    
            data class EUR(
                @SerializedName("name")
                val name: String?, // Euro
                @SerializedName("symbol")
                val symbol: String? // €
            )
    
            data class FJD(
                @SerializedName("name")
                val name: String?, // Fijian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class FKP(
                @SerializedName("name")
                val name: String?, // Falkland Islands pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class FOK(
                @SerializedName("name")
                val name: String?, // Faroese króna
                @SerializedName("symbol")
                val symbol: String? // kr
            )
    
            data class GBP(
                @SerializedName("name")
                val name: String?, // British pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class GEL(
                @SerializedName("name")
                val name: String?, // lari
                @SerializedName("symbol")
                val symbol: String? // ₾
            )
    
            data class GGP(
                @SerializedName("name")
                val name: String?, // Guernsey pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class GHS(
                @SerializedName("name")
                val name: String?, // Ghanaian cedi
                @SerializedName("symbol")
                val symbol: String? // ₵
            )
    
            data class GIP(
                @SerializedName("name")
                val name: String?, // Gibraltar pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class GMD(
                @SerializedName("name")
                val name: String?, // dalasi
                @SerializedName("symbol")
                val symbol: String? // D
            )
    
            data class GNF(
                @SerializedName("name")
                val name: String?, // Guinean franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class GTQ(
                @SerializedName("name")
                val name: String?, // Guatemalan quetzal
                @SerializedName("symbol")
                val symbol: String? // Q
            )
    
            data class GYD(
                @SerializedName("name")
                val name: String?, // Guyanese dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class HKD(
                @SerializedName("name")
                val name: String?, // Hong Kong dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class HNL(
                @SerializedName("name")
                val name: String?, // Honduran lempira
                @SerializedName("symbol")
                val symbol: String? // L
            )
    
            data class HTG(
                @SerializedName("name")
                val name: String?, // Haitian gourde
                @SerializedName("symbol")
                val symbol: String? // G
            )
    
            data class HUF(
                @SerializedName("name")
                val name: String?, // Hungarian forint
                @SerializedName("symbol")
                val symbol: String? // Ft
            )
    
            data class IDR(
                @SerializedName("name")
                val name: String?, // Indonesian rupiah
                @SerializedName("symbol")
                val symbol: String? // Rp
            )
    
            data class ILS(
                @SerializedName("name")
                val name: String?, // Israeli new shekel
                @SerializedName("symbol")
                val symbol: String? // ₪
            )
    
            data class IMP(
                @SerializedName("name")
                val name: String?, // Manx pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class INR(
                @SerializedName("name")
                val name: String?, // Indian rupee
                @SerializedName("symbol")
                val symbol: String? // ₹
            )
    
            data class IQD(
                @SerializedName("name")
                val name: String?, // Iraqi dinar
                @SerializedName("symbol")
                val symbol: String? // ع.د
            )
    
            data class IRR(
                @SerializedName("name")
                val name: String?, // Iranian rial
                @SerializedName("symbol")
                val symbol: String? // ﷼
            )
    
            data class ISK(
                @SerializedName("name")
                val name: String?, // Icelandic króna
                @SerializedName("symbol")
                val symbol: String? // kr
            )
    
            data class JEP(
                @SerializedName("name")
                val name: String?, // Jersey pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class JMD(
                @SerializedName("name")
                val name: String?, // Jamaican dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class JOD(
                @SerializedName("name")
                val name: String?, // Jordanian dinar
                @SerializedName("symbol")
                val symbol: String? // د.ا
            )
    
            data class JPY(
                @SerializedName("name")
                val name: String?, // Japanese yen
                @SerializedName("symbol")
                val symbol: String? // ¥
            )
    
            data class KES(
                @SerializedName("name")
                val name: String?, // Kenyan shilling
                @SerializedName("symbol")
                val symbol: String? // Sh
            )
    
            data class KGS(
                @SerializedName("name")
                val name: String?, // Kyrgyzstani som
                @SerializedName("symbol")
                val symbol: String? // с
            )
    
            data class KHR(
                @SerializedName("name")
                val name: String?, // Cambodian riel
                @SerializedName("symbol")
                val symbol: String? // ៛
            )
    
            data class KID(
                @SerializedName("name")
                val name: String?, // Kiribati dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class KMF(
                @SerializedName("name")
                val name: String?, // Comorian franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class KPW(
                @SerializedName("name")
                val name: String?, // North Korean won
                @SerializedName("symbol")
                val symbol: String? // ₩
            )
    
            data class KRW(
                @SerializedName("name")
                val name: String?, // South Korean won
                @SerializedName("symbol")
                val symbol: String? // ₩
            )
    
            data class KWD(
                @SerializedName("name")
                val name: String?, // Kuwaiti dinar
                @SerializedName("symbol")
                val symbol: String? // د.ك
            )
    
            data class KYD(
                @SerializedName("name")
                val name: String?, // Cayman Islands dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class KZT(
                @SerializedName("name")
                val name: String?, // Kazakhstani tenge
                @SerializedName("symbol")
                val symbol: String? // ₸
            )
    
            data class LAK(
                @SerializedName("name")
                val name: String?, // Lao kip
                @SerializedName("symbol")
                val symbol: String? // ₭
            )
    
            data class LBP(
                @SerializedName("name")
                val name: String?, // Lebanese pound
                @SerializedName("symbol")
                val symbol: String? // ل.ل
            )
    
            data class LKR(
                @SerializedName("name")
                val name: String?, // Sri Lankan rupee
                @SerializedName("symbol")
                val symbol: String? // Rs  රු
            )
    
            data class LRD(
                @SerializedName("name")
                val name: String?, // Liberian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class LSL(
                @SerializedName("name")
                val name: String?, // Lesotho loti
                @SerializedName("symbol")
                val symbol: String? // L
            )
    
            data class LYD(
                @SerializedName("name")
                val name: String?, // Libyan dinar
                @SerializedName("symbol")
                val symbol: String? // ل.د
            )
    
            data class MAD(
                @SerializedName("name")
                val name: String?, // Moroccan dirham
                @SerializedName("symbol")
                val symbol: String? // DH
            )
    
            data class MDL(
                @SerializedName("name")
                val name: String?, // Moldovan leu
                @SerializedName("symbol")
                val symbol: String? // L
            )
    
            data class MGA(
                @SerializedName("name")
                val name: String?, // Malagasy ariary
                @SerializedName("symbol")
                val symbol: String? // Ar
            )
    
            data class MKD(
                @SerializedName("name")
                val name: String?, // denar
                @SerializedName("symbol")
                val symbol: String? // den
            )
    
            data class MMK(
                @SerializedName("name")
                val name: String?, // Burmese kyat
                @SerializedName("symbol")
                val symbol: String? // Ks
            )
    
            data class MNT(
                @SerializedName("name")
                val name: String?, // Mongolian tögrög
                @SerializedName("symbol")
                val symbol: String? // ₮
            )
    
            data class MOP(
                @SerializedName("name")
                val name: String?, // Macanese pataca
                @SerializedName("symbol")
                val symbol: String? // P
            )
    
            data class MRU(
                @SerializedName("name")
                val name: String?, // Mauritanian ouguiya
                @SerializedName("symbol")
                val symbol: String? // UM
            )
    
            data class MUR(
                @SerializedName("name")
                val name: String?, // Mauritian rupee
                @SerializedName("symbol")
                val symbol: String? // ₨
            )
    
            data class MVR(
                @SerializedName("name")
                val name: String?, // Maldivian rufiyaa
                @SerializedName("symbol")
                val symbol: String? // .ރ
            )
    
            data class MWK(
                @SerializedName("name")
                val name: String?, // Malawian kwacha
                @SerializedName("symbol")
                val symbol: String? // MK
            )
    
            data class MXN(
                @SerializedName("name")
                val name: String?, // Mexican peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class MYR(
                @SerializedName("name")
                val name: String?, // Malaysian ringgit
                @SerializedName("symbol")
                val symbol: String? // RM
            )
    
            data class MZN(
                @SerializedName("name")
                val name: String?, // Mozambican metical
                @SerializedName("symbol")
                val symbol: String? // MT
            )
    
            data class NAD(
                @SerializedName("name")
                val name: String?, // Namibian dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class NGN(
                @SerializedName("name")
                val name: String?, // Nigerian naira
                @SerializedName("symbol")
                val symbol: String? // ₦
            )
    
            data class NIO(
                @SerializedName("name")
                val name: String?, // Nicaraguan córdoba
                @SerializedName("symbol")
                val symbol: String? // C$
            )
    
            data class NOK(
                @SerializedName("name")
                val name: String?, // krone
                @SerializedName("symbol")
                val symbol: String? // kr
            )
    
            data class NPR(
                @SerializedName("name")
                val name: String?, // Nepalese rupee
                @SerializedName("symbol")
                val symbol: String? // ₨
            )
    
            data class NZD(
                @SerializedName("name")
                val name: String?, // New Zealand dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class OMR(
                @SerializedName("name")
                val name: String?, // Omani rial
                @SerializedName("symbol")
                val symbol: String? // ر.ع.
            )
    
            data class PAB(
                @SerializedName("name")
                val name: String?, // Panamanian balboa
                @SerializedName("symbol")
                val symbol: String? // B/.
            )
    
            data class PEN(
                @SerializedName("name")
                val name: String?, // Peruvian sol
                @SerializedName("symbol")
                val symbol: String? // S/ 
            )
    
            data class PGK(
                @SerializedName("name")
                val name: String?, // Papua New Guinean kina
                @SerializedName("symbol")
                val symbol: String? // K
            )
    
            data class PHP(
                @SerializedName("name")
                val name: String?, // Philippine peso
                @SerializedName("symbol")
                val symbol: String? // ₱
            )
    
            data class PKR(
                @SerializedName("name")
                val name: String?, // Pakistani rupee
                @SerializedName("symbol")
                val symbol: String? // ₨
            )
    
            data class PLN(
                @SerializedName("name")
                val name: String?, // Polish złoty
                @SerializedName("symbol")
                val symbol: String? // zł
            )
    
            data class PYG(
                @SerializedName("name")
                val name: String?, // Paraguayan guaraní
                @SerializedName("symbol")
                val symbol: String? // ₲
            )
    
            data class QAR(
                @SerializedName("name")
                val name: String?, // Qatari riyal
                @SerializedName("symbol")
                val symbol: String? // ر.ق
            )
    
            data class RON(
                @SerializedName("name")
                val name: String?, // Romanian leu
                @SerializedName("symbol")
                val symbol: String? // lei
            )
    
            data class RSD(
                @SerializedName("name")
                val name: String?, // Serbian dinar
                @SerializedName("symbol")
                val symbol: String? // дин.
            )
    
            data class RUB(
                @SerializedName("name")
                val name: String?, // Russian ruble
                @SerializedName("symbol")
                val symbol: String? // ₽
            )
    
            data class RWF(
                @SerializedName("name")
                val name: String?, // Rwandan franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class SAR(
                @SerializedName("name")
                val name: String?, // Saudi riyal
                @SerializedName("symbol")
                val symbol: String? // ر.س
            )
    
            data class SBD(
                @SerializedName("name")
                val name: String?, // Solomon Islands dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class SCR(
                @SerializedName("name")
                val name: String?, // Seychellois rupee
                @SerializedName("symbol")
                val symbol: String? // ₨
            )
    
            data class SDG(
                @SerializedName("name")
                val name: String? // Sudanese pound
            )
    
            data class SEK(
                @SerializedName("name")
                val name: String?, // Swedish krona
                @SerializedName("symbol")
                val symbol: String? // kr
            )
    
            data class SGD(
                @SerializedName("name")
                val name: String?, // Singapore dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class SHP(
                @SerializedName("name")
                val name: String?, // Saint Helena pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class SLL(
                @SerializedName("name")
                val name: String?, // Sierra Leonean leone
                @SerializedName("symbol")
                val symbol: String? // Le
            )
    
            data class SOS(
                @SerializedName("name")
                val name: String?, // Somali shilling
                @SerializedName("symbol")
                val symbol: String? // Sh
            )
    
            data class SRD(
                @SerializedName("name")
                val name: String?, // Surinamese dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class SSP(
                @SerializedName("name")
                val name: String?, // South Sudanese pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class STN(
                @SerializedName("name")
                val name: String?, // São Tomé and Príncipe dobra
                @SerializedName("symbol")
                val symbol: String? // Db
            )
    
            data class SYP(
                @SerializedName("name")
                val name: String?, // Syrian pound
                @SerializedName("symbol")
                val symbol: String? // £
            )
    
            data class SZL(
                @SerializedName("name")
                val name: String?, // Swazi lilangeni
                @SerializedName("symbol")
                val symbol: String? // L
            )
    
            data class THB(
                @SerializedName("name")
                val name: String?, // Thai baht
                @SerializedName("symbol")
                val symbol: String? // ฿
            )
    
            data class TJS(
                @SerializedName("name")
                val name: String?, // Tajikistani somoni
                @SerializedName("symbol")
                val symbol: String? // ЅМ
            )
    
            data class TMT(
                @SerializedName("name")
                val name: String?, // Turkmenistan manat
                @SerializedName("symbol")
                val symbol: String? // m
            )
    
            data class TND(
                @SerializedName("name")
                val name: String?, // Tunisian dinar
                @SerializedName("symbol")
                val symbol: String? // د.ت
            )
    
            data class TOP(
                @SerializedName("name")
                val name: String?, // Tongan paʻanga
                @SerializedName("symbol")
                val symbol: String? // T$
            )
    
            data class TRY(
                @SerializedName("name")
                val name: String?, // Turkish lira
                @SerializedName("symbol")
                val symbol: String? // ₺
            )
    
            data class TTD(
                @SerializedName("name")
                val name: String?, // Trinidad and Tobago dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class TVD(
                @SerializedName("name")
                val name: String?, // Tuvaluan dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class TWD(
                @SerializedName("name")
                val name: String?, // New Taiwan dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class TZS(
                @SerializedName("name")
                val name: String?, // Tanzanian shilling
                @SerializedName("symbol")
                val symbol: String? // Sh
            )
    
            data class UAH(
                @SerializedName("name")
                val name: String?, // Ukrainian hryvnia
                @SerializedName("symbol")
                val symbol: String? // ₴
            )
    
            data class UGX(
                @SerializedName("name")
                val name: String?, // Ugandan shilling
                @SerializedName("symbol")
                val symbol: String? // Sh
            )
    
            data class USD(
                @SerializedName("name")
                val name: String?, // United States dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class UYU(
                @SerializedName("name")
                val name: String?, // Uruguayan peso
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class UZS(
                @SerializedName("name")
                val name: String?, // Uzbekistani soʻm
                @SerializedName("symbol")
                val symbol: String? // so'm
            )
    
            data class VES(
                @SerializedName("name")
                val name: String?, // Venezuelan bolívar soberano
                @SerializedName("symbol")
                val symbol: String? // Bs.S.
            )
    
            data class VND(
                @SerializedName("name")
                val name: String?, // Vietnamese đồng
                @SerializedName("symbol")
                val symbol: String? // ₫
            )
    
            data class VUV(
                @SerializedName("name")
                val name: String?, // Vanuatu vatu
                @SerializedName("symbol")
                val symbol: String? // Vt
            )
    
            data class WST(
                @SerializedName("name")
                val name: String?, // Samoan tālā
                @SerializedName("symbol")
                val symbol: String? // T
            )
    
            data class XAF(
                @SerializedName("name")
                val name: String?, // Central African CFA franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class XCD(
                @SerializedName("name")
                val name: String?, // Eastern Caribbean dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
    
            data class XOF(
                @SerializedName("name")
                val name: String?, // West African CFA franc
                @SerializedName("symbol")
                val symbol: String? // Fr
            )
    
            data class XPF(
                @SerializedName("name")
                val name: String?, // CFP franc
                @SerializedName("symbol")
                val symbol: String? // ₣
            )
    
            data class YER(
                @SerializedName("name")
                val name: String?, // Yemeni rial
                @SerializedName("symbol")
                val symbol: String? // ﷼
            )
    
            data class ZAR(
                @SerializedName("name")
                val name: String?, // South African rand
                @SerializedName("symbol")
                val symbol: String? // R
            )
    
            data class ZMW(
                @SerializedName("name")
                val name: String?, // Zambian kwacha
                @SerializedName("symbol")
                val symbol: String? // ZK
            )
    
            data class ZWL(
                @SerializedName("name")
                val name: String?, // Zimbabwean dollar
                @SerializedName("symbol")
                val symbol: String? // $
            )
        }
    
        data class Demonyms(
            @SerializedName("eng")
            val eng: Eng?,
            @SerializedName("fra")
            val fra: Fra?
        ) {
            data class Eng(
                @SerializedName("f")
                val f: String?, // Barbadian
                @SerializedName("m")
                val m: String? // Barbadian
            )
    
            data class Fra(
                @SerializedName("f")
                val f: String?, // Barbadienne
                @SerializedName("m")
                val m: String? // Barbadien
            )
        }
    
        data class Flags(
            @SerializedName("alt")
            val alt: String?, // The flag of Barbados is composed of three equal vertical bands of ultramarine, gold and ultramarine. The head of a black trident is centered in the gold band.
            @SerializedName("png")
            val png: String?, // https://flagcdn.com/w320/bb.png
            @SerializedName("svg")
            val svg: String? // https://flagcdn.com/bb.svg
        )

    
        data class Idd(
            @SerializedName("root")
            val root: String?, // +1
            @SerializedName("suffixes")
            val suffixes: List<String?>?
        )
    
        data class Languages(
            @SerializedName("afr")
            val afr: String?, // Afrikaans
            @SerializedName("amh")
            val amh: String?, // Amharic
            @SerializedName("ara")
            val ara: String?, // Arabic
            @SerializedName("arc")
            val arc: String?, // Aramaic
            @SerializedName("aym")
            val aym: String?, // Aymara
            @SerializedName("aze")
            val aze: String?, // Azerbaijani
            @SerializedName("bel")
            val bel: String?, // Belarusian
            @SerializedName("ben")
            val ben: String?, // Bengali
            @SerializedName("ber")
            val ber: String?, // Berber
            @SerializedName("bis")
            val bis: String?, // Bislama
            @SerializedName("bjz")
            val bjz: String?, // Belizean Creole
            @SerializedName("bos")
            val bos: String?, // Bosnian
            @SerializedName("bul")
            val bul: String?, // Bulgarian
            @SerializedName("bwg")
            val bwg: String?, // Chibarwe
            @SerializedName("cal")
            val cal: String?, // Carolinian
            @SerializedName("cat")
            val cat: String?, // Catalan
            @SerializedName("ces")
            val ces: String?, // Czech
            @SerializedName("cha")
            val cha: String?, // Chamorro
            @SerializedName("ckb")
            val ckb: String?, // Sorani
            @SerializedName("cnr")
            val cnr: String?, // Montenegrin
            @SerializedName("crs")
            val crs: String?, // Seychellois Creole
            @SerializedName("dan")
            val dan: String?, // Danish
            @SerializedName("de")
            val de: String?, // German
            @SerializedName("deu")
            val deu: String?, // German
            @SerializedName("div")
            val div: String?, // Maldivian
            @SerializedName("dzo")
            val dzo: String?, // Dzongkha
            @SerializedName("ell")
            val ell: String?, // Greek
            @SerializedName("eng")
            val eng: String?, // English
            @SerializedName("est")
            val est: String?, // Estonian
            @SerializedName("fao")
            val fao: String?, // Faroese
            @SerializedName("fas")
            val fas: String?, // Persian (Farsi)
            @SerializedName("fij")
            val fij: String?, // Fijian
            @SerializedName("fil")
            val fil: String?, // Filipino
            @SerializedName("fin")
            val fin: String?, // Finnish
            @SerializedName("fra")
            val fra: String?, // French
            @SerializedName("gil")
            val gil: String?, // Gilbertese
            @SerializedName("gle")
            val gle: String?, // Irish
            @SerializedName("glv")
            val glv: String?, // Manx
            @SerializedName("grn")
            val grn: String?, // Guaraní
            @SerializedName("gsw")
            val gsw: String?, // Swiss German
            @SerializedName("hat")
            val hat: String?, // Haitian Creole
            @SerializedName("heb")
            val heb: String?, // Hebrew
            @SerializedName("her")
            val her: String?, // Herero
            @SerializedName("hgm")
            val hgm: String?, // Khoekhoe
            @SerializedName("hif")
            val hif: String?, // Fiji Hindi
            @SerializedName("hin")
            val hin: String?, // Hindi
            @SerializedName("hmo")
            val hmo: String?, // Hiri Motu
            @SerializedName("hrv")
            val hrv: String?, // Croatian
            @SerializedName("hun")
            val hun: String?, // Hungarian
            @SerializedName("hye")
            val hye: String?, // Armenian
            @SerializedName("ind")
            val ind: String?, // Indonesian
            @SerializedName("isl")
            val isl: String?, // Icelandic
            @SerializedName("ita")
            val ita: String?, // Italian
            @SerializedName("jam")
            val jam: String?, // Jamaican Patois
            @SerializedName("jpn")
            val jpn: String?, // Japanese
            @SerializedName("kal")
            val kal: String?, // Greenlandic
            @SerializedName("kat")
            val kat: String?, // Georgian
            @SerializedName("kaz")
            val kaz: String?, // Kazakh
            @SerializedName("kck")
            val kck: String?, // Kalanga
            @SerializedName("khi")
            val khi: String?, // Khoisan
            @SerializedName("khm")
            val khm: String?, // Khmer
            @SerializedName("kin")
            val kin: String?, // Kinyarwanda
            @SerializedName("kir")
            val kir: String?, // Kyrgyz
            @SerializedName("kon")
            val kon: String?, // Kikongo
            @SerializedName("kor")
            val kor: String?, // Korean
            @SerializedName("kwn")
            val kwn: String?, // Kwangali
            @SerializedName("lao")
            val lao: String?, // Lao
            @SerializedName("lat")
            val lat: String?, // Latin
            @SerializedName("lav")
            val lav: String?, // Latvian
            @SerializedName("lin")
            val lin: String?, // Lingala
            @SerializedName("lit")
            val lit: String?, // Lithuanian
            @SerializedName("loz")
            val loz: String?, // Lozi
            @SerializedName("ltz")
            val ltz: String?, // Luxembourgish
            @SerializedName("lua")
            val lua: String?, // Tshiluba
            @SerializedName("mah")
            val mah: String?, // Marshallese
            @SerializedName("mey")
            val mey: String?, // Hassaniya
            @SerializedName("mfe")
            val mfe: String?, // Mauritian Creole
            @SerializedName("mkd")
            val mkd: String?, // Macedonian
            @SerializedName("mlg")
            val mlg: String?, // Malagasy
            @SerializedName("mlt")
            val mlt: String?, // Maltese
            @SerializedName("mon")
            val mon: String?, // Mongolian
            @SerializedName("mri")
            val mri: String?, // Māori
            @SerializedName("msa")
            val msa: String?, // Malay
            @SerializedName("mya")
            val mya: String?, // Burmese
            @SerializedName("nau")
            val nau: String?, // Nauru
            @SerializedName("nbl")
            val nbl: String?, // Southern Ndebele
            @SerializedName("ndc")
            val ndc: String?, // Ndau
            @SerializedName("nde")
            val nde: String?, // Northern Ndebele
            @SerializedName("ndo")
            val ndo: String?, // Ndonga
            @SerializedName("nep")
            val nep: String?, // Nepali
            @SerializedName("nfr")
            val nfr: String?, // Guernésiais
            @SerializedName("niu")
            val niu: String?, // Niuean
            @SerializedName("nld")
            val nld: String?, // Dutch
            @SerializedName("nno")
            val nno: String?, // Norwegian Nynorsk
            @SerializedName("nob")
            val nob: String?, // Norwegian Bokmål
            @SerializedName("nor")
            val nor: String?, // Norwegian
            @SerializedName("nrf")
            val nrf: String?, // Jèrriais
            @SerializedName("nso")
            val nso: String?, // Northern Sotho
            @SerializedName("nya")
            val nya: String?, // Chewa
            @SerializedName("nzs")
            val nzs: String?, // New Zealand Sign Language
            @SerializedName("pap")
            val pap: String?, // Papiamento
            @SerializedName("pau")
            val pau: String?, // Palauan
            @SerializedName("pih")
            val pih: String?, // Norfuk
            @SerializedName("pol")
            val pol: String?, // Polish
            @SerializedName("por")
            val por: String?, // Portuguese
            @SerializedName("pov")
            val pov: String?, // Upper Guinea Creole
            @SerializedName("prs")
            val prs: String?, // Dari
            @SerializedName("pus")
            val pus: String?, // Pashto
            @SerializedName("que")
            val que: String?, // Quechua
            @SerializedName("rar")
            val rar: String?, // Cook Islands Māori
            @SerializedName("roh")
            val roh: String?, // Romansh
            @SerializedName("ron")
            val ron: String?, // Romanian
            @SerializedName("run")
            val run: String?, // Kirundi
            @SerializedName("rus")
            val rus: String?, // Russian
            @SerializedName("sag")
            val sag: String?, // Sango
            @SerializedName("sin")
            val sin: String?, // Sinhala
            @SerializedName("slk")
            val slk: String?, // Slovak
            @SerializedName("slv")
            val slv: String?, // Slovene
            @SerializedName("smi")
            val smi: String?, // Sami
            @SerializedName("smo")
            val smo: String?, // Samoan
            @SerializedName("sna")
            val sna: String?, // Shona
            @SerializedName("som")
            val som: String?, // Somali
            @SerializedName("sot")
            val sot: String?, // Southern Sotho
            @SerializedName("spa")
            val spa: String?, // Spanish
            @SerializedName("sqi")
            val sqi: String?, // Albanian
            @SerializedName("srp")
            val srp: String?, // Serbian
            @SerializedName("ssw")
            val ssw: String?, // Swazi
            @SerializedName("swa")
            val swa: String?, // Swahili
            @SerializedName("swe")
            val swe: String?, // Swedish
            @SerializedName("tam")
            val tam: String?, // Tamil
            @SerializedName("tet")
            val tet: String?, // Tetum
            @SerializedName("tgk")
            val tgk: String?, // Tajik
            @SerializedName("tha")
            val tha: String?, // Thai
            @SerializedName("tir")
            val tir: String?, // Tigrinya
            @SerializedName("tkl")
            val tkl: String?, // Tokelauan
            @SerializedName("toi")
            val toi: String?, // Tonga
            @SerializedName("ton")
            val ton: String?, // Tongan
            @SerializedName("tpi")
            val tpi: String?, // Tok Pisin
            @SerializedName("tsn")
            val tsn: String?, // Tswana
            @SerializedName("tso")
            val tso: String?, // Tsonga
            @SerializedName("tuk")
            val tuk: String?, // Turkmen
            @SerializedName("tur")
            val tur: String?, // Turkish
            @SerializedName("tvl")
            val tvl: String?, // Tuvaluan
            @SerializedName("ukr")
            val ukr: String?, // Ukrainian
            @SerializedName("urd")
            val urd: String?, // Urdu
            @SerializedName("uzb")
            val uzb: String?, // Uzbek
            @SerializedName("ven")
            val ven: String?, // Venda
            @SerializedName("vie")
            val vie: String?, // Vietnamese
            @SerializedName("xho")
            val xho: String?, // Xhosa
            @SerializedName("zdj")
            val zdj: String?, // Comorian
            @SerializedName("zho")
            val zho: String?, // Chinese
            @SerializedName("zib")
            val zib: String?, // Zimbabwean Sign Language
            @SerializedName("zul")
            val zul: String? // Zulu
        )
    
        data class Maps(
            @SerializedName("googleMaps")
            val googleMaps: String?, // https://goo.gl/maps/2m36v8STvbGAWd9c7
            @SerializedName("openStreetMaps")
            val openStreetMaps: String? // https://www.openstreetmap.org/relation/547511
        )
    
        data class Name(
            @SerializedName("common")
            val common: String?, // Barbados
            @SerializedName("nativeName")
            val nativeName: NativeName?,
            @SerializedName("official")
            val official: String? // Barbados
        ) {
            data class NativeName(
                @SerializedName("afr")
                val afr: Afr?,
                @SerializedName("amh")
                val amh: Amh?,
                @SerializedName("ara")
                val ara: Ara?,
                @SerializedName("arc")
                val arc: Arc?,
                @SerializedName("aym")
                val aym: Aym?,
                @SerializedName("aze")
                val aze: Aze?,
                @SerializedName("bar")
                val bar: Bar?,
                @SerializedName("bel")
                val bel: Bel?,
                @SerializedName("ben")
                val ben: Ben?,
                @SerializedName("ber")
                val ber: Ber?,
                @SerializedName("bis")
                val bis: Bis?,
                @SerializedName("bjz")
                val bjz: Bjz?,
                @SerializedName("bos")
                val bos: Bos?,
                @SerializedName("bul")
                val bul: Bul?,
                @SerializedName("bwg")
                val bwg: Bwg?,
                @SerializedName("cal")
                val cal: Cal?,
                @SerializedName("cat")
                val cat: Cat?,
                @SerializedName("ces")
                val ces: Ces?,
                @SerializedName("cha")
                val cha: Cha?,
                @SerializedName("ckb")
                val ckb: Ckb?,
                @SerializedName("cnr")
                val cnr: Cnr?,
                @SerializedName("crs")
                val crs: Crs?,
                @SerializedName("dan")
                val dan: Dan?,
                @SerializedName("deu")
                val deu: Deu?,
                @SerializedName("div")
                val div: Div?,
                @SerializedName("dzo")
                val dzo: Dzo?,
                @SerializedName("ell")
                val ell: Ell?,
                @SerializedName("eng")
                val eng: Eng?,
                @SerializedName("est")
                val est: Est?,
                @SerializedName("fao")
                val fao: Fao?,
                @SerializedName("fas")
                val fas: Fas?,
                @SerializedName("fij")
                val fij: Fij?,
                @SerializedName("fil")
                val fil: Fil?,
                @SerializedName("fin")
                val fin: Fin?,
                @SerializedName("fra")
                val fra: Fra?,
                @SerializedName("gil")
                val gil: Gil?,
                @SerializedName("gle")
                val gle: Gle?,
                @SerializedName("glv")
                val glv: Glv?,
                @SerializedName("grn")
                val grn: Grn?,
                @SerializedName("gsw")
                val gsw: Gsw?,
                @SerializedName("hat")
                val hat: Hat?,
                @SerializedName("heb")
                val heb: Heb?,
                @SerializedName("her")
                val her: Her?,
                @SerializedName("hgm")
                val hgm: Hgm?,
                @SerializedName("hif")
                val hif: Hif?,
                @SerializedName("hin")
                val hin: Hin?,
                @SerializedName("hmo")
                val hmo: Hmo?,
                @SerializedName("hrv")
                val hrv: Hrv?,
                @SerializedName("hun")
                val hun: Hun?,
                @SerializedName("hye")
                val hye: Hye?,
                @SerializedName("ind")
                val ind: Ind?,
                @SerializedName("isl")
                val isl: Isl?,
                @SerializedName("ita")
                val ita: Ita?,
                @SerializedName("jam")
                val jam: Jam?,
                @SerializedName("jpn")
                val jpn: Jpn?,
                @SerializedName("kal")
                val kal: Kal?,
                @SerializedName("kat")
                val kat: Kat?,
                @SerializedName("kaz")
                val kaz: Kaz?,
                @SerializedName("kck")
                val kck: Kck?,
                @SerializedName("khi")
                val khi: Khi?,
                @SerializedName("khm")
                val khm: Khm?,
                @SerializedName("kin")
                val kin: Kin?,
                @SerializedName("kir")
                val kir: Kir?,
                @SerializedName("kon")
                val kon: Kon?,
                @SerializedName("kor")
                val kor: Kor?,
                @SerializedName("kwn")
                val kwn: Kwn?,
                @SerializedName("lao")
                val lao: Lao?,
                @SerializedName("lat")
                val lat: Lat?,
                @SerializedName("lav")
                val lav: Lav?,
                @SerializedName("lin")
                val lin: Lin?,
                @SerializedName("lit")
                val lit: Lit?,
                @SerializedName("loz")
                val loz: Loz?,
                @SerializedName("ltz")
                val ltz: Ltz?,
                @SerializedName("lua")
                val lua: Lua?,
                @SerializedName("mah")
                val mah: Mah?,
                @SerializedName("mey")
                val mey: Mey?,
                @SerializedName("mfe")
                val mfe: Mfe?,
                @SerializedName("mkd")
                val mkd: Mkd?,
                @SerializedName("mlg")
                val mlg: Mlg?,
                @SerializedName("mlt")
                val mlt: Mlt?,
                @SerializedName("mon")
                val mon: Mon?,
                @SerializedName("mri")
                val mri: Mri?,
                @SerializedName("msa")
                val msa: Msa?,
                @SerializedName("mya")
                val mya: Mya?,
                @SerializedName("nau")
                val nau: Nau?,
                @SerializedName("nbl")
                val nbl: Nbl?,
                @SerializedName("ndc")
                val ndc: Ndc?,
                @SerializedName("nde")
                val nde: Nde?,
                @SerializedName("ndo")
                val ndo: Ndo?,
                @SerializedName("nep")
                val nep: Nep?,
                @SerializedName("nfr")
                val nfr: Nfr?,
                @SerializedName("niu")
                val niu: Niu?,
                @SerializedName("nld")
                val nld: Nld?,
                @SerializedName("nno")
                val nno: Nno?,
                @SerializedName("nob")
                val nob: Nob?,
                @SerializedName("nor")
                val nor: Nor?,
                @SerializedName("nrf")
                val nrf: Nrf?,
                @SerializedName("nso")
                val nso: Nso?,
                @SerializedName("nya")
                val nya: Nya?,
                @SerializedName("nzs")
                val nzs: Nzs?,
                @SerializedName("pap")
                val pap: Pap?,
                @SerializedName("pau")
                val pau: Pau?,
                @SerializedName("pih")
                val pih: Pih?,
                @SerializedName("pol")
                val pol: Pol?,
                @SerializedName("por")
                val por: Por?,
                @SerializedName("pov")
                val pov: Pov?,
                @SerializedName("prs")
                val prs: Prs?,
                @SerializedName("pus")
                val pus: Pus?,
                @SerializedName("que")
                val que: Que?,
                @SerializedName("rar")
                val rar: Rar?,
                @SerializedName("roh")
                val roh: Roh?,
                @SerializedName("ron")
                val ron: Ron?,
                @SerializedName("run")
                val run: Run?,
                @SerializedName("rus")
                val rus: Rus?,
                @SerializedName("sag")
                val sag: Sag?,
                @SerializedName("sin")
                val sin: Sin?,
                @SerializedName("slk")
                val slk: Slk?,
                @SerializedName("slv")
                val slv: Slv?,
                @SerializedName("smi")
                val smi: Smi?,
                @SerializedName("smo")
                val smo: Smo?,
                @SerializedName("sna")
                val sna: Sna?,
                @SerializedName("som")
                val som: Som?,
                @SerializedName("sot")
                val sot: Sot?,
                @SerializedName("spa")
                val spa: Spa?,
                @SerializedName("sqi")
                val sqi: Sqi?,
                @SerializedName("srp")
                val srp: Srp?,
                @SerializedName("ssw")
                val ssw: Ssw?,
                @SerializedName("swa")
                val swa: Swa?,
                @SerializedName("swe")
                val swe: Swe?,
                @SerializedName("tam")
                val tam: Tam?,
                @SerializedName("tet")
                val tet: Tet?,
                @SerializedName("tgk")
                val tgk: Tgk?,
                @SerializedName("tha")
                val tha: Tha?,
                @SerializedName("tir")
                val tir: Tir?,
                @SerializedName("tkl")
                val tkl: Tkl?,
                @SerializedName("toi")
                val toi: Toi?,
                @SerializedName("ton")
                val ton: Ton?,
                @SerializedName("tpi")
                val tpi: Tpi?,
                @SerializedName("tsn")
                val tsn: Tsn?,
                @SerializedName("tso")
                val tso: Tso?,
                @SerializedName("tuk")
                val tuk: Tuk?,
                @SerializedName("tur")
                val tur: Tur?,
                @SerializedName("tvl")
                val tvl: Tvl?,
                @SerializedName("ukr")
                val ukr: Ukr?,
                @SerializedName("urd")
                val urd: Urd?,
                @SerializedName("uzb")
                val uzb: Uzb?,
                @SerializedName("ven")
                val ven: Ven?,
                @SerializedName("vie")
                val vie: Vie?,
                @SerializedName("xho")
                val xho: Xho?,
                @SerializedName("zdj")
                val zdj: Zdj?,
                @SerializedName("zho")
                val zho: Zho?,
                @SerializedName("zib")
                val zib: Zib?,
                @SerializedName("zul")
                val zul: Zul?
            ) {
                data class Afr(
                    @SerializedName("common")
                    val common: String?, // Namibië
                    @SerializedName("official")
                    val official: String? // Republiek van Namibië
                )
    
                data class Amh(
                    @SerializedName("common")
                    val common: String?, // ኢትዮጵያ
                    @SerializedName("official")
                    val official: String? // የኢትዮጵያ ፌዴራላዊ ዲሞክራሲያዊ ሪፐብሊክ
                )
    
                data class Ara(
                    @SerializedName("common")
                    val common: String?, // مصر
                    @SerializedName("official")
                    val official: String? // جمهورية مصر العربية
                )
    
                data class Arc(
                    @SerializedName("common")
                    val common: String?, // ܩܘܼܛܢܵܐ
                    @SerializedName("official")
                    val official: String? // ܩܘܼܛܢܵܐ ܐܝܼܪܲܩ
                )
    
                data class Aym(
                    @SerializedName("common")
                    val common: String?, // Piruw
                    @SerializedName("official")
                    val official: String? // Piruw Suyu
                )
    
                data class Aze(
                    @SerializedName("common")
                    val common: String?, // Azərbaycan
                    @SerializedName("official")
                    val official: String? // Azərbaycan Respublikası
                )
    
                data class Bar(
                    @SerializedName("common")
                    val common: String?, // Österreich
                    @SerializedName("official")
                    val official: String? // Republik Österreich
                )
    
                data class Bel(
                    @SerializedName("common")
                    val common: String?, // Белару́сь
                    @SerializedName("official")
                    val official: String? // Рэспубліка Беларусь
                )
    
                data class Ben(
                    @SerializedName("common")
                    val common: String?, // বাংলাদেশ
                    @SerializedName("official")
                    val official: String? // বাংলাদেশ গণপ্রজাতন্ত্রী
                )
    
                data class Ber(
                    @SerializedName("common")
                    val common: String?, // Western Sahara
                    @SerializedName("official")
                    val official: String? // Sahrawi Arab Democratic Republic
                )
    
                data class Bis(
                    @SerializedName("common")
                    val common: String?, // Vanuatu
                    @SerializedName("official")
                    val official: String? // Ripablik blong Vanuatu
                )
    
                data class Bjz(
                    @SerializedName("common")
                    val common: String?, // Belize
                    @SerializedName("official")
                    val official: String? // Belize
                )
    
                data class Bos(
                    @SerializedName("common")
                    val common: String?, // Bosna i Hercegovina
                    @SerializedName("official")
                    val official: String? // Bosna i Hercegovina
                )
    
                data class Bul(
                    @SerializedName("common")
                    val common: String?, // България
                    @SerializedName("official")
                    val official: String? // Република България
                )
    
                data class Bwg(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Cal(
                    @SerializedName("common")
                    val common: String?, // Northern Mariana Islands
                    @SerializedName("official")
                    val official: String? // Commonwealth of the Northern Mariana Islands
                )
    
                data class Cat(
                    @SerializedName("common")
                    val common: String?, // Andorra
                    @SerializedName("official")
                    val official: String? // Principat d'Andorra
                )
    
                data class Ces(
                    @SerializedName("common")
                    val common: String?, // Česko
                    @SerializedName("official")
                    val official: String? // Česká republika
                )
    
                data class Cha(
                    @SerializedName("common")
                    val common: String?, // Na Islas Mariånas
                    @SerializedName("official")
                    val official: String? // Sankattan Siha Na Islas Mariånas
                )
    
                data class Ckb(
                    @SerializedName("common")
                    val common: String?, // کۆماری
                    @SerializedName("official")
                    val official: String? // کۆماری عێراق
                )
    
                data class Cnr(
                    @SerializedName("common")
                    val common: String?, // Црна Гора
                    @SerializedName("official")
                    val official: String? // Црна Гора
                )
    
                data class Crs(
                    @SerializedName("common")
                    val common: String?, // Sesel
                    @SerializedName("official")
                    val official: String? // Repiblik Sesel
                )
    
                data class Dan(
                    @SerializedName("common")
                    val common: String?, // Danmark
                    @SerializedName("official")
                    val official: String? // Kongeriget Danmark
                )
    
                data class Deu(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Republik Namibia
                )
    
                data class Div(
                    @SerializedName("common")
                    val common: String?, // ދިވެހިރާއްޖޭގެ
                    @SerializedName("official")
                    val official: String? // ދިވެހިރާއްޖޭގެ ޖުމްހޫރިއްޔާ
                )
    
                data class Dzo(
                    @SerializedName("common")
                    val common: String?, // འབྲུག་ཡུལ་
                    @SerializedName("official")
                    val official: String? // འབྲུག་རྒྱལ་ཁབ་
                )
    
                data class Ell(
                    @SerializedName("common")
                    val common: String?, // Κύπρος
                    @SerializedName("official")
                    val official: String? // Δημοκρατία της Κύπρος
                )
    
                data class Eng(
                    @SerializedName("common")
                    val common: String?, // Barbados
                    @SerializedName("official")
                    val official: String? // Barbados
                )
    
                data class Est(
                    @SerializedName("common")
                    val common: String?, // Eesti
                    @SerializedName("official")
                    val official: String? // Eesti Vabariik
                )
    
                data class Fao(
                    @SerializedName("common")
                    val common: String?, // Føroyar
                    @SerializedName("official")
                    val official: String? // Føroyar
                )
    
                data class Fas(
                    @SerializedName("common")
                    val common: String?, // ایران
                    @SerializedName("official")
                    val official: String? // جمهوری اسلامی ایران
                )
    
                data class Fij(
                    @SerializedName("common")
                    val common: String?, // Viti
                    @SerializedName("official")
                    val official: String? // Matanitu Tugalala o Viti
                )
    
                data class Fil(
                    @SerializedName("common")
                    val common: String?, // Pilipinas
                    @SerializedName("official")
                    val official: String? // Republic of the Philippines
                )
    
                data class Fin(
                    @SerializedName("common")
                    val common: String?, // Suomi
                    @SerializedName("official")
                    val official: String? // Suomen tasavalta
                )
    
                data class Fra(
                    @SerializedName("common")
                    val common: String?, // La Réunion
                    @SerializedName("official")
                    val official: String? // Ile de la Réunion
                )
    
                data class Gil(
                    @SerializedName("common")
                    val common: String?, // Kiribati
                    @SerializedName("official")
                    val official: String? // Ribaberiki Kiribati
                )
    
                data class Gle(
                    @SerializedName("common")
                    val common: String?, // Éire
                    @SerializedName("official")
                    val official: String? // Poblacht na hÉireann
                )
    
                data class Glv(
                    @SerializedName("common")
                    val common: String?, // Mannin
                    @SerializedName("official")
                    val official: String? // Ellan Vannin or Mannin
                )
    
                data class Grn(
                    @SerializedName("common")
                    val common: String?, // Argentina
                    @SerializedName("official")
                    val official: String? // Argentine Republic
                )
    
                data class Gsw(
                    @SerializedName("common")
                    val common: String?, // Schweiz
                    @SerializedName("official")
                    val official: String? // Schweizerische Eidgenossenschaft
                )
    
                data class Hat(
                    @SerializedName("common")
                    val common: String?, // Ayiti
                    @SerializedName("official")
                    val official: String? // Repiblik Ayiti
                )
    
                data class Heb(
                    @SerializedName("common")
                    val common: String?, // ישראל
                    @SerializedName("official")
                    val official: String? // מדינת ישראל
                )
    
                data class Her(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Republic of Namibia
                )
    
                data class Hgm(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Republic of Namibia
                )
    
                data class Hif(
                    @SerializedName("common")
                    val common: String?, // फिजी
                    @SerializedName("official")
                    val official: String? // रिपब्लिक ऑफ फीजी
                )
    
                data class Hin(
                    @SerializedName("common")
                    val common: String?, // भारत
                    @SerializedName("official")
                    val official: String? // भारत गणराज्य
                )
    
                data class Hmo(
                    @SerializedName("common")
                    val common: String?, // Papua Niu Gini
                    @SerializedName("official")
                    val official: String? // Independen Stet bilong Papua Niugini
                )
    
                data class Hrv(
                    @SerializedName("common")
                    val common: String?, // Bosna i Hercegovina
                    @SerializedName("official")
                    val official: String? // Bosna i Hercegovina
                )
    
                data class Hun(
                    @SerializedName("common")
                    val common: String?, // Magyarország
                    @SerializedName("official")
                    val official: String? // Magyarország
                )
    
                data class Hye(
                    @SerializedName("common")
                    val common: String?, // Հայաստան
                    @SerializedName("official")
                    val official: String? // Հայաստանի Հանրապետություն
                )
    
                data class Ind(
                    @SerializedName("common")
                    val common: String?, // Indonesia
                    @SerializedName("official")
                    val official: String? // Republik Indonesia
                )
    
                data class Isl(
                    @SerializedName("common")
                    val common: String?, // Ísland
                    @SerializedName("official")
                    val official: String? // Ísland
                )
    
                data class Ita(
                    @SerializedName("common")
                    val common: String?, // Svizzera
                    @SerializedName("official")
                    val official: String? // Confederazione Svizzera
                )
    
                data class Jam(
                    @SerializedName("common")
                    val common: String?, // Jamaica
                    @SerializedName("official")
                    val official: String? // Jamaica
                )
    
                data class Jpn(
                    @SerializedName("common")
                    val common: String?, // 日本
                    @SerializedName("official")
                    val official: String? // 日本
                )
    
                data class Kal(
                    @SerializedName("common")
                    val common: String?, // Kalaallit Nunaat
                    @SerializedName("official")
                    val official: String? // Kalaallit Nunaat
                )
    
                data class Kat(
                    @SerializedName("common")
                    val common: String?, // საქართველო
                    @SerializedName("official")
                    val official: String? // საქართველო
                )
    
                data class Kaz(
                    @SerializedName("common")
                    val common: String?, // Қазақстан
                    @SerializedName("official")
                    val official: String? // Қазақстан Республикасы
                )
    
                data class Kck(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Khi(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Khm(
                    @SerializedName("common")
                    val common: String?, // Kâmpŭchéa
                    @SerializedName("official")
                    val official: String? // ព្រះរាជាណាចក្រកម្ពុជា
                )
    
                data class Kin(
                    @SerializedName("common")
                    val common: String?, // Rwanda
                    @SerializedName("official")
                    val official: String? // Repubulika y'u Rwanda
                )
    
                data class Kir(
                    @SerializedName("common")
                    val common: String?, // Кыргызстан
                    @SerializedName("official")
                    val official: String? // Кыргыз Республикасы
                )
    
                data class Kon(
                    @SerializedName("common")
                    val common: String?, // Repubilika ya Kongo
                    @SerializedName("official")
                    val official: String? // Repubilika ya Kongo
                )
    
                data class Kor(
                    @SerializedName("common")
                    val common: String?, // 조선
                    @SerializedName("official")
                    val official: String? // 조선민주주의인민공화국
                )
    
                data class Kwn(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Republic of Namibia
                )
    
                data class Lao(
                    @SerializedName("common")
                    val common: String?, // ສປປລາວ
                    @SerializedName("official")
                    val official: String? // ສາທາລະນະ ຊາທິປະໄຕ ຄົນລາວ ຂອງ
                )
    
                data class Lat(
                    @SerializedName("common")
                    val common: String?, // Vaticanæ
                    @SerializedName("official")
                    val official: String? // Status Civitatis Vaticanæ
                )
    
                data class Lav(
                    @SerializedName("common")
                    val common: String?, // Latvija
                    @SerializedName("official")
                    val official: String? // Latvijas Republikas
                )
    
                data class Lin(
                    @SerializedName("common")
                    val common: String?, // Republíki ya Kongó
                    @SerializedName("official")
                    val official: String? // Republíki ya Kongó
                )
    
                data class Lit(
                    @SerializedName("common")
                    val common: String?, // Lietuva
                    @SerializedName("official")
                    val official: String? // Lietuvos Respublikos
                )
    
                data class Loz(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Republic of Namibia
                )
    
                data class Ltz(
                    @SerializedName("common")
                    val common: String?, // Lëtzebuerg
                    @SerializedName("official")
                    val official: String? // Groussherzogtum Lëtzebuerg
                )
    
                data class Lua(
                    @SerializedName("common")
                    val common: String?, // Ditunga dia Kongu wa Mungalaata
                    @SerializedName("official")
                    val official: String? // Ditunga dia Kongu wa Mungalaata
                )
    
                data class Mah(
                    @SerializedName("common")
                    val common: String?, // M̧ajeļ
                    @SerializedName("official")
                    val official: String? // Republic of the Marshall Islands
                )
    
                data class Mey(
                    @SerializedName("common")
                    val common: String?, // الصحراء الغربية
                    @SerializedName("official")
                    val official: String? // الجمهورية العربية الصحراوية الديمقراطية
                )
    
                data class Mfe(
                    @SerializedName("common")
                    val common: String?, // Moris
                    @SerializedName("official")
                    val official: String? // Republik Moris
                )
    
                data class Mkd(
                    @SerializedName("common")
                    val common: String?, // Македонија
                    @SerializedName("official")
                    val official: String? // Република Северна Македонија
                )
    
                data class Mlg(
                    @SerializedName("common")
                    val common: String?, // Madagasikara
                    @SerializedName("official")
                    val official: String? // Repoblikan'i Madagasikara
                )
    
                data class Mlt(
                    @SerializedName("common")
                    val common: String?, // Malta
                    @SerializedName("official")
                    val official: String? // Repubblika ta ' Malta
                )
    
                data class Mon(
                    @SerializedName("common")
                    val common: String?, // Монгол улс
                    @SerializedName("official")
                    val official: String? // Монгол улс
                )
    
                data class Mri(
                    @SerializedName("common")
                    val common: String?, // Aotearoa
                    @SerializedName("official")
                    val official: String? // Aotearoa
                )
    
                data class Msa(
                    @SerializedName("common")
                    val common: String?, // مليسيا
                    @SerializedName("official")
                    val official: String? // مليسيا
                )
    
                data class Mya(
                    @SerializedName("common")
                    val common: String?, // မြန်မာ
                    @SerializedName("official")
                    val official: String? // ပြည်ထောင်စု သမ္မတ မြန်မာနိုင်ငံတော်
                )
    
                data class Nau(
                    @SerializedName("common")
                    val common: String?, // Nauru
                    @SerializedName("official")
                    val official: String? // Republic of Nauru
                )
    
                data class Nbl(
                    @SerializedName("common")
                    val common: String?, // Sewula Afrika
                    @SerializedName("official")
                    val official: String? // IRiphabliki yeSewula Afrika
                )
    
                data class Ndc(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Nde(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Ndo(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Republic of Namibia
                )
    
                data class Nep(
                    @SerializedName("common")
                    val common: String?, // नेपाल
                    @SerializedName("official")
                    val official: String? // नेपाल संघीय लोकतान्त्रिक गणतन्त्र
                )
    
                data class Nfr(
                    @SerializedName("common")
                    val common: String?, // Dgèrnésiais
                    @SerializedName("official")
                    val official: String? // Dgèrnésiais
                )
    
                data class Niu(
                    @SerializedName("common")
                    val common: String?, // Niuē
                    @SerializedName("official")
                    val official: String? // Niuē
                )
    
                data class Nld(
                    @SerializedName("common")
                    val common: String?, // Suriname
                    @SerializedName("official")
                    val official: String? // Republiek Suriname
                )
    
                data class Nno(
                    @SerializedName("common")
                    val common: String?, // Noreg
                    @SerializedName("official")
                    val official: String? // Kongeriket Noreg
                )
    
                data class Nob(
                    @SerializedName("common")
                    val common: String?, // Norge
                    @SerializedName("official")
                    val official: String? // Kongeriket Norge
                )
    
                data class Nor(
                    @SerializedName("common")
                    val common: String?, // Bouvetøya
                    @SerializedName("official")
                    val official: String? // Bouvetøya
                )
    
                data class Nrf(
                    @SerializedName("common")
                    val common: String?, // Jèrri
                    @SerializedName("official")
                    val official: String? // Bailliage dé Jèrri
                )
    
                data class Nso(
                    @SerializedName("common")
                    val common: String?, // Afrika-Borwa
                    @SerializedName("official")
                    val official: String? // Rephaboliki ya Afrika-Borwa 
                )
    
                data class Nya(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Nzs(
                    @SerializedName("common")
                    val common: String?, // New Zealand
                    @SerializedName("official")
                    val official: String? // New Zealand
                )
    
                data class Pap(
                    @SerializedName("common")
                    val common: String?, // Boneiru, Sint Eustatius y Saba
                    @SerializedName("official")
                    val official: String? // Boneiru, Sint Eustatius y Saba
                )
    
                data class Pau(
                    @SerializedName("common")
                    val common: String?, // Belau
                    @SerializedName("official")
                    val official: String? // Beluu er a Belau
                )
    
                data class Pih(
                    @SerializedName("common")
                    val common: String?, // Norf'k Ailen
                    @SerializedName("official")
                    val official: String? // Teratri of Norf'k Ailen
                )
    
                data class Pol(
                    @SerializedName("common")
                    val common: String?, // Polska
                    @SerializedName("official")
                    val official: String? // Rzeczpospolita Polska
                )
    
                data class Por(
                    @SerializedName("common")
                    val common: String?, // Timor-Leste
                    @SerializedName("official")
                    val official: String? // República Democrática de Timor-Leste
                )
    
                data class Pov(
                    @SerializedName("common")
                    val common: String?, // Guiné-Bissau
                    @SerializedName("official")
                    val official: String? // República da Guiné-Bissau
                )
    
                data class Prs(
                    @SerializedName("common")
                    val common: String?, // افغانستان
                    @SerializedName("official")
                    val official: String? // جمهوری اسلامی افغانستان
                )
    
                data class Pus(
                    @SerializedName("common")
                    val common: String?, // افغانستان
                    @SerializedName("official")
                    val official: String? // د افغانستان اسلامي جمهوریت
                )
    
                data class Que(
                    @SerializedName("common")
                    val common: String?, // Piruw
                    @SerializedName("official")
                    val official: String? // Piruw Ripuwlika
                )
    
                data class Rar(
                    @SerializedName("common")
                    val common: String?, // Kūki 'Āirani
                    @SerializedName("official")
                    val official: String? // Kūki 'Āirani
                )
    
                data class Roh(
                    @SerializedName("common")
                    val common: String?, // Svizra
                    @SerializedName("official")
                    val official: String? // Confederaziun svizra
                )
    
                data class Ron(
                    @SerializedName("common")
                    val common: String?, // Moldova
                    @SerializedName("official")
                    val official: String? // Republica Moldova
                )
    
                data class Run(
                    @SerializedName("common")
                    val common: String?, // Uburundi
                    @SerializedName("official")
                    val official: String? // Republika y'Uburundi 
                )
    
                data class Rus(
                    @SerializedName("common")
                    val common: String?, // Азербайджан
                    @SerializedName("official")
                    val official: String? // Азербайджанская Республика
                )
    
                data class Sag(
                    @SerializedName("common")
                    val common: String?, // Bêafrîka
                    @SerializedName("official")
                    val official: String? // Ködörösêse tî Bêafrîka
                )
    
                data class Sin(
                    @SerializedName("common")
                    val common: String?, // ශ්‍රී ලංකාව
                    @SerializedName("official")
                    val official: String? // ශ්‍රී ලංකා ප්‍රජාතාන්ත්‍රික සමාජවාදී ජනරජය
                )
    
                data class Slk(
                    @SerializedName("common")
                    val common: String?, // Česko
                    @SerializedName("official")
                    val official: String? // Česká republika
                )
    
                data class Slv(
                    @SerializedName("common")
                    val common: String?, // Slovenija
                    @SerializedName("official")
                    val official: String? // Republika Slovenija
                )
    
                data class Smi(
                    @SerializedName("common")
                    val common: String?, // Norgga
                    @SerializedName("official")
                    val official: String? // Norgga gonagasriika
                )
    
                data class Smo(
                    @SerializedName("common")
                    val common: String?, // Sāmoa
                    @SerializedName("official")
                    val official: String? // Malo Saʻoloto Tutoʻatasi o Sāmoa
                )
    
                data class Sna(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Som(
                    @SerializedName("common")
                    val common: String?, // Soomaaliya
                    @SerializedName("official")
                    val official: String? // Jamhuuriyadda Federaalka Soomaaliya
                )
    
                data class Sot(
                    @SerializedName("common")
                    val common: String?, // Afrika Borwa
                    @SerializedName("official")
                    val official: String? // Rephaboliki ya Afrika Borwa
                )
    
                data class Spa(
                    @SerializedName("common")
                    val common: String?, // Panamá
                    @SerializedName("official")
                    val official: String? // República de Panamá
                )
    
                data class Sqi(
                    @SerializedName("common")
                    val common: String?, // Shqipëria
                    @SerializedName("official")
                    val official: String? // Republika e Shqipërisë
                )
    
                data class Srp(
                    @SerializedName("common")
                    val common: String?, // Босна и Херцеговина
                    @SerializedName("official")
                    val official: String? // Босна и Херцеговина
                )
    
                data class Ssw(
                    @SerializedName("common")
                    val common: String?, // Ningizimu Afrika
                    @SerializedName("official")
                    val official: String? // IRiphabhulikhi yeNingizimu Afrika
                )
    
                data class Swa(
                    @SerializedName("common")
                    val common: String?, // Jamhuri ya Kidemokrasia ya Kongo
                    @SerializedName("official")
                    val official: String? // Jamhuri ya Kidemokrasia ya Kongo
                )
    
                data class Swe(
                    @SerializedName("common")
                    val common: String?, // Åland
                    @SerializedName("official")
                    val official: String? // Landskapet Åland
                )
    
                data class Tam(
                    @SerializedName("common")
                    val common: String?, // இலங்கை
                    @SerializedName("official")
                    val official: String? // இலங்கை சனநாயக சோசலிசக் குடியரசு
                )
    
                data class Tet(
                    @SerializedName("common")
                    val common: String?, // Timór-Leste
                    @SerializedName("official")
                    val official: String? // Repúblika Demokrátika Timór-Leste
                )
    
                data class Tgk(
                    @SerializedName("common")
                    val common: String?, // Тоҷикистон
                    @SerializedName("official")
                    val official: String? // Ҷумҳурии Тоҷикистон
                )
    
                data class Tha(
                    @SerializedName("common")
                    val common: String?, // ประเทศไทย
                    @SerializedName("official")
                    val official: String? // ราชอาณาจักรไทย
                )
    
                data class Tir(
                    @SerializedName("common")
                    val common: String?, // ኤርትራ
                    @SerializedName("official")
                    val official: String? // ሃገረ ኤርትራ
                )
    
                data class Tkl(
                    @SerializedName("common")
                    val common: String?, // Tokelau
                    @SerializedName("official")
                    val official: String? // Tokelau
                )
    
                data class Toi(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Ton(
                    @SerializedName("common")
                    val common: String?, // Tonga
                    @SerializedName("official")
                    val official: String? // Kingdom of Tonga
                )
    
                data class Tpi(
                    @SerializedName("common")
                    val common: String?, // Papua Niugini
                    @SerializedName("official")
                    val official: String? // Independen Stet bilong Papua Niugini
                )
    
                data class Tsn(
                    @SerializedName("common")
                    val common: String?, // Namibia
                    @SerializedName("official")
                    val official: String? // Lefatshe la Namibia
                )
    
                data class Tso(
                    @SerializedName("common")
                    val common: String?, // Afrika Dzonga
                    @SerializedName("official")
                    val official: String? // Riphabliki ra Afrika Dzonga
                )
    
                data class Tuk(
                    @SerializedName("common")
                    val common: String?, // Owganystan
                    @SerializedName("official")
                    val official: String? // Owganystan Yslam Respublikasy
                )
    
                data class Tur(
                    @SerializedName("common")
                    val common: String?, // Türkiye
                    @SerializedName("official")
                    val official: String? // Türkiye Cumhuriyeti
                )
    
                data class Tvl(
                    @SerializedName("common")
                    val common: String?, // Tuvalu
                    @SerializedName("official")
                    val official: String? // Tuvalu
                )
    
                data class Ukr(
                    @SerializedName("common")
                    val common: String?, // Україна
                    @SerializedName("official")
                    val official: String? // Україна
                )
    
                data class Urd(
                    @SerializedName("common")
                    val common: String?, // پاكستان
                    @SerializedName("official")
                    val official: String? // اسلامی جمہوریۂ پاكستان
                )
    
                data class Uzb(
                    @SerializedName("common")
                    val common: String?, // O‘zbekiston
                    @SerializedName("official")
                    val official: String? // O'zbekiston Respublikasi
                )
    
                data class Ven(
                    @SerializedName("common")
                    val common: String?, // Afurika Tshipembe
                    @SerializedName("official")
                    val official: String? // Riphabuḽiki ya Afurika Tshipembe
                )
    
                data class Vie(
                    @SerializedName("common")
                    val common: String?, // Việt Nam
                    @SerializedName("official")
                    val official: String? // Cộng hòa xã hội chủ nghĩa Việt Nam
                )
    
                data class Xho(
                    @SerializedName("common")
                    val common: String?, // Mzantsi Afrika
                    @SerializedName("official")
                    val official: String? // IRiphabliki yaseMzantsi Afrika
                )
    
                data class Zdj(
                    @SerializedName("common")
                    val common: String?, // Komori
                    @SerializedName("official")
                    val official: String? // Udzima wa Komori
                )
    
                data class Zho(
                    @SerializedName("common")
                    val common: String?, // 香港
                    @SerializedName("official")
                    val official: String? // 中华人民共和国香港特别行政区
                )
    
                data class Zib(
                    @SerializedName("common")
                    val common: String?, // Zimbabwe
                    @SerializedName("official")
                    val official: String? // Republic of Zimbabwe
                )
    
                data class Zul(
                    @SerializedName("common")
                    val common: String?, // Ningizimu Afrika
                    @SerializedName("official")
                    val official: String? // IRiphabliki yaseNingizimu Afrika
                )
            }
        }
    
        data class PostalCode(
            @SerializedName("format")
            val format: String?, // BB#####
            @SerializedName("regex")
            val regex: String? // ^(?:BB)*(\d{5})$
        )
    
        data class Translations(
            @SerializedName("ara")
            val ara: Ara?,
            @SerializedName("bre")
            val bre: Bre?,
            @SerializedName("ces")
            val ces: Ces?,
            @SerializedName("cym")
            val cym: Cym?,
            @SerializedName("deu")
            val deu: Deu?,
            @SerializedName("est")
            val est: Est?,
            @SerializedName("fin")
            val fin: Fin?,
            @SerializedName("fra")
            val fra: Fra?,
            @SerializedName("hrv")
            val hrv: Hrv?,
            @SerializedName("hun")
            val hun: Hun?,
            @SerializedName("ita")
            val ita: Ita?,
            @SerializedName("jpn")
            val jpn: Jpn?,
            @SerializedName("kor")
            val kor: Kor?,
            @SerializedName("nld")
            val nld: Nld?,
            @SerializedName("per")
            val per: Per?,
            @SerializedName("pol")
            val pol: Pol?,
            @SerializedName("por")
            val por: Por?,
            @SerializedName("rus")
            val rus: Rus?,
            @SerializedName("slk")
            val slk: Slk?,
            @SerializedName("spa")
            val spa: Spa?,
            @SerializedName("srp")
            val srp: Srp?,
            @SerializedName("swe")
            val swe: Swe?,
            @SerializedName("tur")
            val tur: Tur?,
            @SerializedName("urd")
            val urd: Urd?,
            @SerializedName("zho")
            val zho: Zho?
        ) {
            data class Ara(
                @SerializedName("common")
                val common: String?, // باربادوس
                @SerializedName("official")
                val official: String? // باربادوس
            )
    
            data class Bre(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Ces(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Cym(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Deu(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Est(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Fin(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Fra(
                @SerializedName("common")
                val common: String?, // Barbade
                @SerializedName("official")
                val official: String? // Barbade
            )
    
            data class Hrv(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Hun(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Ita(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Jpn(
                @SerializedName("common")
                val common: String?, // バルバドス
                @SerializedName("official")
                val official: String? // バルバドス
            )
    
            data class Kor(
                @SerializedName("common")
                val common: String?, // 바베이도스
                @SerializedName("official")
                val official: String? // 바베이도스
            )
    
            data class Nld(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Per(
                @SerializedName("common")
                val common: String?, // باربادوس
                @SerializedName("official")
                val official: String? // باربادوس
            )
    
            data class Pol(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Por(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Rus(
                @SerializedName("common")
                val common: String?, // Барбадос
                @SerializedName("official")
                val official: String? // Барбадос
            )
    
            data class Slk(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Spa(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Srp(
                @SerializedName("common")
                val common: String?, // Барбадос
                @SerializedName("official")
                val official: String? // Барбадос
            )
    
            data class Swe(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Tur(
                @SerializedName("common")
                val common: String?, // Barbados
                @SerializedName("official")
                val official: String? // Barbados
            )
    
            data class Urd(
                @SerializedName("common")
                val common: String?, // بارباڈوس
                @SerializedName("official")
                val official: String? // بارباڈوس
            )
    
            data class Zho(
                @SerializedName("common")
                val common: String?, // 巴巴多斯
                @SerializedName("official")
                val official: String? // 巴巴多斯
            )
        }
    }
}