package com.ema.countries.view.favorites

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ema.countries.R
import com.ema.countries.databinding.FragmentFavoritesBinding
import com.ema.countries.viewmodel.FavoriteViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoritesFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentFavoritesBinding

    //other
    private val viewModel: FavoriteViewModel by viewModels()

    @Inject
    lateinit var adapter: FavoriteAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavoritesBinding.inflate(layoutInflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvFavorite.adapter = adapter
        binding.rvFavorite.layoutManager = LinearLayoutManager(requireContext())

        viewModel.getFavoriteCountries()
        viewModel.listOfFavorites.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            Log.e("favorite", "${it.size}")
        }

        adapter.setOnItemClickListener {
            val action = FavoritesFragmentDirections.actionFavoritesFragmentToDetailFragment(it.id)
            findNavController().navigate(action)
        }
    }


}