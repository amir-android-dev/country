package com.ema.countries.data.repository.favorite

import com.ema.countries.data.db.CountryDao
import com.ema.countries.data.model.EntityFavorite
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface FavoriteRepository {
    fun getAllFavorites(): Flow<List<EntityFavorite>>
}

class FavoriteRepositoryImpl @Inject constructor(private val dao: CountryDao) : FavoriteRepository {
    override fun getAllFavorites(): Flow<List<EntityFavorite>> {
        return dao.getAllFavorites()
    }
}