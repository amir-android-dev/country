package com.ema.countries.viewmodel

import androidx.lifecycle.*
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.data.model.EntityFavorite
import com.ema.countries.data.repository.detail.DetailRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val repository: DetailRepository) : ViewModel() {

    private val _existsFavorite = MutableLiveData<Boolean>()
    val existsFavorite: LiveData<Boolean>
        get() = _existsFavorite

    private val _countryById = MutableLiveData<EntityCountry>()
    val countryById: LiveData<EntityCountry>
        get() = _countryById


    fun insertFavorite(favorite: EntityFavorite) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertFavorite(favorite)
    }

    fun deleteFavorite(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteFavorite(id)
    }

    fun existsFavorite(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.existsFavorite(id).collect {
            _existsFavorite.postValue(it)
        }
    }

    fun getCountryById(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.getCountryById(id).collect {
            _countryById.postValue(it)
        }
    }


}