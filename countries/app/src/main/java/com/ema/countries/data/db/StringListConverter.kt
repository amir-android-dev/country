package com.ema.countries.data.db

import androidx.room.TypeConverter

class StringListConverter {
    @TypeConverter
    fun fromString(value: String?): List<String> {
        return value?.split(",") ?: emptyList()
    }

    @TypeConverter
    fun toString(value: List<String>?): String {
        return value?.joinToString(",") ?: ""
    }
}