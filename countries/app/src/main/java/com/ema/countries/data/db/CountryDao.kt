package com.ema.countries.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.data.model.EntityFavorite
import com.ema.countries.utils.COUNTRY_TABLE
import com.ema.countries.utils.FAVORITE_TABLE
import kotlinx.coroutines.flow.Flow

@Dao
interface CountryDao {
    @Insert
    suspend fun insertCountry(country: EntityCountry)

    @Query("select * from $COUNTRY_TABLE")
    fun getAllCountries(): Flow<List<EntityCountry>>

    @Query("SELECT * FROM $COUNTRY_TABLE WHERE id = :id")
    fun getCountryById(id: Int): Flow<EntityCountry>

    @Query("SELECT * FROM $COUNTRY_TABLE WHERE officialName LIKE '%' || :letter || '%'")
    fun searchCountryByName(letter: String): Flow<List<EntityCountry>>

    @Query("SELECT * FROM $COUNTRY_TABLE WHERE officialName LIKE :letter || '%'")
    fun getCountriesByChar(letter: String): Flow<List<EntityCountry>>

    @Query("DELETE FROM $COUNTRY_TABLE")
    suspend fun deleteAllCountries()

    //favorite
    @Insert
    suspend fun insertFavorite(country: EntityFavorite)

    @Query("SELECT * FROM $FAVORITE_TABLE")
    fun getAllFavorites(): Flow<List<EntityFavorite>>

    @Query("delete from $FAVORITE_TABLE where id = :id")
    suspend fun deleteFavorite(id: Int)

    @Query("SELECT EXISTS (SELECT 1 FROM $FAVORITE_TABLE WHERE id = :id)")
    fun existsFavorite(id: Int): Flow<Boolean>
}