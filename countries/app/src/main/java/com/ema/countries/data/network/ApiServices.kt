package com.ema.countries.data.network

import com.ema.countries.data.model.ResponseAllCountries
import retrofit2.Response
import retrofit2.http.GET

interface ApiServices {
    @GET("all")
   suspend fun getAllCountries(): Response<ResponseAllCountries>
}