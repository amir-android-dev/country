package com.ema.countries.view.countries

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.databinding.ItemCountryBinding
import com.ema.countries.view.countries.CountriesAdapter.MViewHolder
import javax.inject.Inject

class CountriesAdapter @Inject constructor() :
    ListAdapter<EntityCountry, MViewHolder>(object :
        DiffUtil.ItemCallback<EntityCountry>() {
        override fun areItemsTheSame(
            oldItem: EntityCountry,
            newItem: EntityCountry
        ) = oldItem.officialName == newItem.officialName

        override fun areContentsTheSame(
            oldItem: EntityCountry,
            newItem: EntityCountry
        ) = oldItem == newItem
    }) {
    private lateinit var binding: ItemCountryBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding = ItemCountryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(currentList[position])
        holder.setIsRecyclable(false)
    }

    inner class MViewHolder : ViewHolder(binding.root) {

        fun bind(country: EntityCountry) {
            binding.tvCountryName.text = country.officialName
            binding.ivMap.load(country.flags){
                crossfade(300)
                crossfade(true)
            }
            binding.tvCapitalCity.text = country.region


            //click listener
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(country) }
            }
        }
    }

    private var onItemClickListener: ((EntityCountry) -> Unit)? = null
    fun setOnItemClickListener(listener: (EntityCountry) -> Unit) {
        onItemClickListener = listener
    }

}