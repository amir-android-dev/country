package com.ema.countries.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ema.countries.utils.FAVORITE_TABLE
import java.util.UUID

@Entity(tableName = FAVORITE_TABLE)
data class EntityFavorite(
    @PrimaryKey(autoGenerate = false)
    var uuid: UUID,
    var id: Int = 0,
    var commonName: String = "",
    var img: String = ""
)