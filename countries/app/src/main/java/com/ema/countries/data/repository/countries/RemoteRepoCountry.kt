package com.ema.countries.data.repository.countries

import com.ema.countries.data.model.ResponseAllCountries
import com.ema.countries.data.network.ApiServices
import com.ema.countries.utils.MyResponse
import com.ema.countries.utils.handleApiCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

interface RemoteRepoCountry {
    suspend fun getAllCountries(): Flow<MyResponse<ResponseAllCountries>>
    suspend fun getAllCountries2(): Flow<MyResponse<ResponseAllCountries>>

}

class RemoteRepoCountryImpl @Inject constructor(private val apiServices: ApiServices) :
    RemoteRepoCountry {

    override suspend fun getAllCountries(): Flow<MyResponse<ResponseAllCountries>> {
        return handleApiCall { apiServices.getAllCountries() }.flowOn(Dispatchers.IO)
    }

    override suspend fun getAllCountries2(): Flow<MyResponse<ResponseAllCountries>> {
        return flow { }
    }

}