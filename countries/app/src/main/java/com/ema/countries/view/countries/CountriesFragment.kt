package com.ema.countries.view.countries

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ema.countries.databinding.FragmentCountriesBinding
import com.ema.countries.utils.CheckConnection
import com.ema.countries.utils.setupListWithAdapter
import com.ema.countries.viewmodel.CountriesViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CountriesFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentCountriesBinding

    //other
    private val viewModel: CountriesViewModel by viewModels()

    @Inject
    lateinit var adapter: CountriesAdapter

    @Inject
    lateinit var categoryAdapter: CategoryAdapter

    @Inject
    lateinit var connection: CheckConnection
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCountriesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllCountries()
        viewModel.insertCountry()
        //check internet connection
        connection.observe(viewLifecycleOwner) { isConnected ->
            if(!isConnected){
                Snackbar.make(
                    requireView(),
                    "PLEASE CHECK YOUR INTERNET CONNECTION",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
        //load countries
        viewModel.listOfCountries.observe(viewLifecycleOwner) { countries ->
                    binding.rvCountries.layoutManager = LinearLayoutManager(requireContext())
                    binding.rvCountries.adapter = adapter
                    adapter.submitList(countries)
                    Log.e("core", "${countries.size}")

            }

        adapter.setOnItemClickListener {
            navigateToDetails(it.id)
        }
        //search
        binding.etSearch.addTextChangedListener {
            viewModel.searchCountriesByName(it.toString())
        }
        //countries by category
        val alphabets = ('A'..'Z').map { it.toString() }
        binding.spinnerBar.setupListWithAdapter(alphabets.toMutableList(), callback = {
            viewModel.getCountriesByChar(binding.spinnerBar.selectedItem.toString())
            viewModel.listOfCategories.observe(viewLifecycleOwner) {
                binding.rvCategory.layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                binding.rvCategory.adapter = categoryAdapter
                categoryAdapter.submitList(it)
            }
        })
        categoryAdapter.setOnItemClickListener {
            navigateToDetails(it.id)
        }
    }

    private fun navigateToDetails(id: Int) {
        val action = CountriesFragmentDirections.actionCountriesFragmentToDetailFragment(id)
        findNavController().navigate(action)
    }

}