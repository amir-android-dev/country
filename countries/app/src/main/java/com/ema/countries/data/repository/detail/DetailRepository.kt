package com.ema.countries.data.repository.detail


import com.ema.countries.data.db.CountryDao
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.data.model.EntityFavorite
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface DetailRepository {

    suspend fun insertFavorite(country: EntityFavorite)

    suspend fun deleteFavorite(id: Int)

    fun existsFavorite(id: Int): Flow<Boolean>

    fun getCountryById(id: Int): Flow<EntityCountry>
}

class DetailRepositoryImpl @Inject constructor(private val dao: CountryDao) : DetailRepository {
    override suspend fun insertFavorite(country: EntityFavorite) = dao.insertFavorite(country)

    override suspend fun deleteFavorite(id: Int) = dao.deleteFavorite(id)

    override fun existsFavorite(id: Int) = dao.existsFavorite(id)
    override fun getCountryById(id: Int) = dao.getCountryById(id)


}