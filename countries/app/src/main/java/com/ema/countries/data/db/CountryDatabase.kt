package com.ema.countries.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.data.model.EntityFavorite

@Database(entities = [EntityCountry::class,EntityFavorite::class], version = 1, exportSchema = false)
@TypeConverters(StringListConverter::class)
abstract class CountryDatabase : RoomDatabase() {
    abstract fun dao(): CountryDao
}