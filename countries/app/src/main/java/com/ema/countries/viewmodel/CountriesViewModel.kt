package com.ema.countries.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.data.repository.countries.CoreRepoCountry
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CountriesViewModel @Inject constructor(private val coreRepo: CoreRepoCountry) :
    ViewModel() {


    private val _listOfCountries = MutableLiveData<List<EntityCountry>>()
    val listOfCountries: LiveData<List<EntityCountry>>
        get() = _listOfCountries

    private val _listOfCategories= MutableLiveData<List<EntityCountry>>()
    val listOfCategories: LiveData<List<EntityCountry>>
        get() = _listOfCategories


    fun searchCountriesByName(letter: String) = viewModelScope.launch {
        coreRepo.searchCountryByName(letter).collect {
            _listOfCountries.postValue(it)
        }
    }

    fun getCountriesByChar(char: String) = viewModelScope.launch {
        coreRepo.getCountriesByChar(char).collect {
            _listOfCategories.postValue(it)
        }
    }

    fun getAllCountries() = viewModelScope.launch(Dispatchers.IO) {
        coreRepo.getAllCountries().collect {
            Log.e("viewmodel", "${it.size}")
            _listOfCountries.postValue(it)
        }
    }

    fun insertCountry() = viewModelScope.launch(Dispatchers.IO) {
        coreRepo.loadAndInsertCountry()
    }


}