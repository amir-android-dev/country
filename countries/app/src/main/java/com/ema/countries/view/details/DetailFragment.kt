package com.ema.countries.view.details

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.ema.countries.MainActivity
import com.ema.countries.R
import com.ema.countries.data.model.EntityFavorite
import com.ema.countries.databinding.FragmentDetailBinding
import com.ema.countries.viewmodel.DetailViewModel
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import java.util.UUID

@AndroidEntryPoint
class DetailFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentDetailBinding

    //other
    private val args: DetailFragmentArgs by navArgs()
    private var isFavorite = false
    private val viewModel: DetailViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.existsFavorite(args.id)
        viewModel.existsFavorite.observe(viewLifecycleOwner) {
            //to set the color of heart(if is favorite the color change)
            if (it) {
                isFavorite = it
                binding.ivFavorite.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.dark_blue
                    )
                )
            } else {
                binding.ivFavorite.setColorFilter(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.light_gray
                    )
                )
            }
        }

        //navigate back
        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }
        //get details
        viewModel.getCountryById(args.id)
        viewModel.countryById.observe(viewLifecycleOwner) { entityCountry ->
            binding.ivFlag.load(entityCountry.flags)
            binding.tvOfficialName.text = entityCountry.officialName
            ifNoDataExist(entityCountry.commonName, "Common Name", binding.tvCommonName)
            ifNoDataExist(entityCountry.capitalCity, "Capital City", binding.tvCapitalCity)
            ifNoDataExist(entityCountry.region, "Region", binding.tvRegion)
            ifNoDataExist(entityCountry.subRegion, "Sub Region", binding.tvSubRegion)
            ifNoDataExist(entityCountry.language, "Language", binding.tvLanguage)
            ifNoDataExist(entityCountry.borders.toString(), "Border", binding.tvBorder)
            ifNoDataExist(entityCountry.population, "Population", binding.tvPopulation)
            ifNoDataExist(entityCountry.fifa, "FIFA", binding.tvFifa)
            ifNoDataExist(entityCountry.carSign, "Car Sign", binding.tvCarSign)
            ifNoDataExist(entityCountry.continents, "Continents", binding.tvContinents)
            ifNoDataExist(entityCountry.flag, "Flag", binding.tvFlag)
            ifNoDataExist(entityCountry.startOfWeek, "Start of the Week", binding.tvStartOfWeek)
            ifNoDataExist(entityCountry.alt, "Alt", binding.tvAlt)
            ifNoDataExist(
                entityCountry.dialingCodeRoot + " " + entityCountry.dialingCodeSuffix,
                "Dialing Code",
                binding.tvCode
            )

            // insert and delete a favorite country
            binding.ivFavorite.setOnClickListener {
                if (isFavorite) {
                    viewModel.deleteFavorite(args.id)
                } else {
                    viewModel.insertFavorite(
                        EntityFavorite(
                            UUID.randomUUID(),
                            entityCountry.id,
                            entityCountry.commonName.toString(),
                            entityCountry.flags.toString()
                        )
                    )
                }
            }
        }
    }

    private fun ifNoDataExist(detail: String?, title: String, textView: MaterialTextView) {
        val modifyDetail = detail?.replace("[", "")?.replace("]", "")

        if (detail.isNullOrEmpty() || detail.contains("null")) {
            textView.text = SpannableStringBuilder("$title: ").bold { append("") }
        } else {
            textView.text = SpannableStringBuilder("$title: ").bold { append(modifyDetail) }
        }
    }


}
//control the title. it will be displayed just when the collaps is closed
/*
        binding.loCollapsing.setExpandedTitleColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.black
            )
        )*/
