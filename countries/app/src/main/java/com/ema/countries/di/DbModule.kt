package com.ema.countries.di

import android.content.Context
import androidx.room.Room
import com.ema.countries.data.db.CountryDatabase
import com.ema.countries.utils.COUNTRY_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DbModule {
    @Provides
    @Singleton
    fun daoCountry(database: CountryDatabase) = database.dao()

    @Provides
    @Singleton
    fun countryDb(@ApplicationContext context: Context) = Room.databaseBuilder(
        context, CountryDatabase::class.java,
        COUNTRY_DATABASE
    )
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration()
        .build()
}