package com.ema.countries.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.countries.data.model.EntityFavorite
import com.ema.countries.data.repository.favorite.FavoriteRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(private val repo: FavoriteRepository) :
    ViewModel() {

    private val _listOfFavorites = MutableLiveData<List<EntityFavorite>>()
    val listOfFavorites: LiveData<List<EntityFavorite>>
        get() = _listOfFavorites

    fun getFavoriteCountries() = viewModelScope.launch(Dispatchers.IO) {
        repo.getAllFavorites().collect {
            _listOfFavorites.postValue(it)
            Log.e("favorite", "${it.size}" )
        }
    }
}