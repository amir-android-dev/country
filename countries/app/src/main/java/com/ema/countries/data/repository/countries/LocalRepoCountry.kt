package com.ema.countries.data.repository.countries

import com.ema.countries.data.db.CountryDao
import com.ema.countries.data.model.EntityCountry
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface LocalRepoCountry {

    suspend fun insertCountry(country: EntityCountry)
    fun getAllCountry(): Flow<List<EntityCountry>>
    suspend fun deleteAllCountries()
    fun searchCountryByName(letter: String): Flow<List<EntityCountry>>
    fun getCountriesByChar(char: String): Flow<List<EntityCountry>>
}

class LocalRepoCountryImpl @Inject constructor(private val dao: CountryDao) : LocalRepoCountry {
    override suspend fun insertCountry(country: EntityCountry) = dao.insertCountry(country)

    override fun getAllCountry() = dao.getAllCountries()
    override suspend fun deleteAllCountries() = dao.deleteAllCountries()
    override fun searchCountryByName(letter: String) = dao.searchCountryByName(letter)
    override fun getCountriesByChar(char: String) = dao.getCountriesByChar(char)
}