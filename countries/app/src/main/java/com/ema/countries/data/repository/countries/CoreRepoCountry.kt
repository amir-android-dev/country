package com.ema.countries.data.repository.countries


import android.util.Log
import com.ema.countries.data.model.EntityCountry
import com.ema.countries.utils.MyResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import javax.inject.Inject

interface CoreRepoCountry {
    suspend fun loadAndInsertCountry()
    fun getAllCountries(): Flow<List<EntityCountry>>
    fun searchCountryByName(letter: String): Flow<List<EntityCountry>>
    fun getCountriesByChar(char: String): Flow<List<EntityCountry>>

}

class CoreRepoCountryImpl @Inject constructor(
    private val localRepo: LocalRepoCountry,
    private val remoteRepo: RemoteRepoCountry
) : CoreRepoCountry {

    override suspend fun loadAndInsertCountry() {
        remoteRepo.getAllCountries().collect { response ->
            when (response.status) {
                MyResponse.Status.LOADING -> {}

                MyResponse.Status.SUCCESS -> {
                    val newCountries = response.data ?: emptyList()
                    val countriesInDb = localRepo.getAllCountry()
                        .first() // Retrieve all the countries in the DB once
                    newCountries.forEach { country ->
                        Log.e("core", country.name?.official.toString())
                        // Check if the country already exists in the DB
                        val existsInDb =
                            countriesInDb.any { it.officialName == country.name?.official }
                        if (!existsInDb) {
                            localRepo.insertCountry(
                                EntityCountry(
                                    id = 0,
                                    commonName = country.name?.common.toString(),
                                    officialName = country.name?.official.toString(),
                                    capitalCity = country.capital.toString(),
                                    subRegion = country.subregion.toString(),
                                    region = country.region.toString(),
                                    language = country.languages?.eng.toString(),
                                    flags = country.flags?.png.toString(),
                                    dialingCodeRoot = country.idd?.root.toString(),
                                    dialingCodeSuffix = country.idd?.suffixes.toString(),
                                    borders = country.borders,
                                    flag = country.flag.toString(),
                                    population = country.population.toString(),
                                    fifa = country.fifa.toString(),
                                    carSign = country.car?.signs.toString(),
                                    continents = country.continents.toString(),
                                    alt = country.flags?.alt.toString(),
                                    coatOfArms = country.coatOfArms?.png.toString(),
                                    startOfWeek = country.startOfWeek.toString()
                                )
                            )
                        }
                    }
                }
                MyResponse.Status.ERROR -> {
                    response.message.toString()
                }
            }
        }
    }

    override fun getAllCountries() = localRepo.getAllCountry()
    override fun searchCountryByName(letter: String) = localRepo.searchCountryByName(letter)
    override fun getCountriesByChar(char: String) = localRepo.getCountriesByChar(char)

}




//delete an insert
    /*override suspend fun loadAndInsertCountry() {

        remoteRepo.getAllCountries().collect { response ->
            when (response.status) {
                MyResponse.Status.LOADING -> {}

                MyResponse.Status.SUCCESS -> {
                    localRepo.deleteAllCountries()
                    val newCountries = response.data ?: emptyList()
                    newCountries.forEach { country ->
                        Log.e("name core", "insert ${country.name?.official.toString()}")
                        try {
                            localRepo.insertCountry(
                                EntityCountry(
                                    commonName = country.name?.common.toString(),
                                    officialName = country.name?.official.toString(),
                                    capitalCity = country.capital.toString(),
                                    subRegion = country.subregion.toString(),
                                    region = country.region.toString(),
                                    language = country.languages?.eng.toString(),
                                    flags = country.flags?.png.toString(),
                                    dialingCodeRoot = country.idd?.root.toString(),
                                    dialingCodeSuffix = country.idd?.suffixes.toString(),
                                    borders = country.borders,
                                    flag = country.flag.toString(),
                                    population = country.population!!,
                                    fifa = country.fifa.toString(),
                                    carSign = country.car?.signs.toString(),
                                    continents = country.continents.toString(),
                                    alt = country.flags?.alt.toString(),
                                    coatOfArms = country.coatOfArms?.png.toString(),
                                    startOfWeek = country.startOfWeek.toString()
                                )
                            )
                        } catch (e: Exception) {
                            Log.e("core insert", e.message.toString())
                        }

                    }
                }
                MyResponse.Status.ERROR -> {
                    response.message.toString()
                }
            }
        }
    }*/