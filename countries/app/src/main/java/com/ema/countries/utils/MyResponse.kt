package com.ema.countries.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Response

data class MyResponse<out T>(
    val status: Status,
    val data: T? = null,
    val message: String? = null
) {
    enum class Status {
        LOADING,
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> loading(): MyResponse<T> {
            return MyResponse(Status.LOADING)
        }

        fun <T> success(data: T?): MyResponse<T> {
            return MyResponse(Status.SUCCESS, data)
        }

        fun <T> error(error: String): MyResponse<T> {
            return MyResponse(Status.ERROR, message = error)
        }
    }
}

suspend fun <T> handleApiCall(apiCall: suspend () -> Response<T>): Flow<MyResponse<T>> {
    return flow {
        val response = apiCall.invoke()
        when (response.code()) {
            in 200..202 -> emit(MyResponse.success(response.body()))
            in 400..499 -> emit(MyResponse.error(response.message()))
            in 500..599 -> emit(MyResponse.error(response.message()))
        }
    }.catch {
        emit(MyResponse.error(it.message.toString()))
    }
}


suspend fun <T> handleApiCall(apiCall: suspend () -> Response<T>, handleSuccess: (T) -> Unit): Flow<MyResponse<T>> {
    return flow {
        val response = apiCall.invoke()
        when (response.code()) {
            in 200..202 -> {
                handleSuccess(response.body()!!)
                emit(MyResponse.success(response.body()))
            }
            in 400..499 -> emit(MyResponse.error(response.message()))
            in 500..599 -> emit(MyResponse.error(response.message()))
        }
    }.catch {
        emit(MyResponse.error(it.message.toString()))
    }
}