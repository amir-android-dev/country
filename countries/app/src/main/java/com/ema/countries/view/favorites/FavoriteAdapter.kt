package com.ema.countries.view.favorites

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.ema.countries.data.model.EntityFavorite
import com.ema.countries.databinding.ItemFavoriteBinding
import com.ema.countries.view.favorites.FavoriteAdapter.MViewHolder
import javax.inject.Inject

class FavoriteAdapter @Inject constructor() : ListAdapter<EntityFavorite, MViewHolder>(object :
    DiffUtil.ItemCallback<EntityFavorite>() {
    override fun areItemsTheSame(oldItem: EntityFavorite, newItem: EntityFavorite) =
        oldItem.uuid == newItem.uuid

    override fun areContentsTheSame(oldItem: EntityFavorite, newItem: EntityFavorite) =
        oldItem == newItem
}) {
    //binding
    private lateinit var binding: ItemFavoriteBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding = ItemFavoriteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(currentList[position])
        holder.setIsRecyclable(true)
    }

    inner class MViewHolder : ViewHolder(binding.root) {
        fun bind(item: EntityFavorite) {
            binding.ivMap.load(item.img) {
                crossfade(500)
                crossfade(true)
            }
            binding.tvCountryName.text = item.commonName

            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item) }
            }
        }
    }

    private var onItemClickListener: ((EntityFavorite) -> Unit)? = null
    fun setOnItemClickListener(listener: (EntityFavorite) -> Unit) {
        onItemClickListener = listener
    }
}