package com.ema.countries.di

import com.ema.countries.data.db.CountryDao
import com.ema.countries.data.network.ApiServices
import com.ema.countries.data.repository.countries.*
import com.ema.countries.data.repository.detail.DetailRepository
import com.ema.countries.data.repository.detail.DetailRepositoryImpl
import com.ema.countries.data.repository.favorite.FavoriteRepository
import com.ema.countries.data.repository.favorite.FavoriteRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RemoteRepoCountryModule {
    @Provides
    fun provideRemoteRepoCountry(apiServices: ApiServices): RemoteRepoCountry {
        return RemoteRepoCountryImpl(apiServices)
    }

    @Provides
    fun provideLocalRepoCountry(dao: CountryDao): LocalRepoCountry {
        return LocalRepoCountryImpl(dao)
    }

    @Provides
    fun provideCoreRepoCountry(
        localRepoCountry: LocalRepoCountryImpl,
        remoteRepoCountry: RemoteRepoCountryImpl
    ): CoreRepoCountry {
        return CoreRepoCountryImpl(localRepo = localRepoCountry, remoteRepo = remoteRepoCountry)
    }


    //detail
    @Provides
    @Singleton
    fun provideDetailRepo(dao: CountryDao): DetailRepository = DetailRepositoryImpl(dao)

    @Provides
    @Singleton
    fun provideFavoriteRepo(dao: CountryDao): FavoriteRepository = FavoriteRepositoryImpl(dao)

}