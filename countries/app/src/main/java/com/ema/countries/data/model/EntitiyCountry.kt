package com.ema.countries.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ema.countries.utils.COUNTRY_TABLE

@Entity(tableName = COUNTRY_TABLE)
data class EntityCountry(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val commonName: String?,
    val officialName: String?,
    val capitalCity: String?,
    val region: String?,
    val subRegion: String?,
    val language: String?,
    val flags: String?,
    val dialingCodeRoot: String?,
    val dialingCodeSuffix: String?,
    val borders: List<String>?,
    val flag: String?,
    val population: String?,
    val fifa: String?,
    val carSign: String?,
    val continents: String?,
    val alt: String?,
    val coatOfArms: String?,
    val startOfWeek: String?,

)